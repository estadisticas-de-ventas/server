FROM node:14.14

WORKDIR /home/node/app
COPY . /home/node/app/
RUN npm install

CMD npm start
EXPOSE 9000
