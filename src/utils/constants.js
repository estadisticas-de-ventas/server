require('dotenv').config();

const BASE_PATH = "https://datawarehouse.violettacosmeticos.com/api";
const API_VERSION = "v1";

module.exports = {
    BASE_PATH,
    API_VERSION,
};