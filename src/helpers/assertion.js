/**
 * Checks if param is undefined, empty or null
 *
 * @param {String|Object|Array} param       - Param to check
 * @return {boolean}                        - True if param fails any of this check. False otherwise
 */
export const doesNotExist = param => {
  if (typeof param === 'undefined' || param === '' || param === null) return true;
  if (Array.isArray(param) && param.length === 0) return true;
  if (typeof param === 'object' && Object.prototype.toString.call(param) !== '[object Date]') {
    return Object.keys(param).length === 0;
  }
  return false;
};

export const exists = param => !doesNotExist(param);

export const exist = (...args) => args.every(a => exists(a));

export const doNotExist = (...args) => args.every(a => doesNotExist(a));

export const anyDoNotExist = (...args) => args.some(a => doesNotExist(a));

export const isEmpty = param => {
  if (param === null || param === undefined) {
    return true;
  }
  if (typeof param === 'object') {
    return Object.keys(param).length === 0 && param.constructor === Object;
  }
  if (Array.isArray(param)) {
    return param.length > 0;
  }
  throw new Error(`isEmpty parameter case of use not found: '${param}'`);
};

export const isNotEmpty = param => !isEmpty(param);