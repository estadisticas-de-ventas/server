const cron = require('node-cron');
const { Pool } = require('pg');
require('dotenv').config();


const postgreSQLConfig = {
  user: process.env.PG_USER,
  host: process.env.PG_HOST,
  database: process.env.PG_DATABASE,
  password: process.env.PG_PASSWORD,
  port: process.env.PG_PORT,
};


const pgPool = new Pool(postgreSQLConfig);

async function updateOrCreate() {
  try {

    // Este es el query para ejecutar la función "asegurar_todas_secciones_zonas"
    const query = 'SELECT asegurar_todas_secciones_zonas();';

    // Ejecutar el query
    const res = await pgPool.query(query);

    console.log('Función ejecutada con éxito:');
  } catch (err) {
    // Si ocurre algún error, lo capturamos
    console.error('Error al ejecutar la función:', err);
  }
}

// Opción para cerrar el pool cuando se detenga el proceso
process.on('SIGINT', async () => {
  await pgPool.end();
  console.log('Conexiones cerradas. El proceso ha finalizado.');
  process.exit();
});


module.exports = { updateOrCreate };