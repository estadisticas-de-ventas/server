const cron = require('node-cron'); // Importamos la librería node-cron
const mysql = require('mysql2/promise');
const { Pool } = require('pg');
const config = require('dotenv').config();


// Configuración de las bases de datos
const mariaDBConfig = {
    host: process.env.MARIADB_HOST,
    user: process.env.MARIADB_USER,
    password: process.env.MARIADB_PASSWORD,
    database: process.env.MARIADB_DATABASE, 
    port: process.env.MARIADB_PORT,
  };
  
  
const postgreSQLConfig = {
    user: process.env.PG_USER,
    host: process.env.PG_HOST,
    database: process.env.PG_DATABASE,
    password: process.env.PG_PASSWORD,
    port: process.env.PG_PORT,
  };
  


async function sincronizarUsuarios() {
  try {
    console.log('Iniciando sincronización de usuarios...');

    // Conexión a MariaDB
    const mariaDBConnection = await mysql.createConnection(mariaDBConfig);
    console.log('Conectado a MariaDB');

    // Conexión a PostgreSQL
    const postgreSQLPool = new Pool(postgreSQLConfig);
    const postgreSQLClient = await postgreSQLPool.connect();
    console.log('Conectado a PostgreSQL');

    // Obtener usuarios de MariaDB
    const [mariaDBUsers] = await mariaDBConnection.query("SELECT * FROM usuarios WHERE estado <> '04' AND tipo_cuenta IN ('ZDPR', 'ZDGE', 'ZDLI', 'ZDGN', 'ZDJV', 'ZDAS', 'ZDCO')");
    console.log(`Usuarios obtenidos de MariaDB: ${mariaDBUsers.length}`);
    
    // Obtener usuarios existentes en PostgreSQL
    const { rows: existingUsers } = await postgreSQLClient.query('SELECT reseller_number FROM users');
    const existingUsersMap = new Map(existingUsers.map(user => [user.reseller_number, user]));
    console.log(`Usuarios existentes en PostgreSQL: ${existingUsersMap.size}`);

    // Sincronización (solo usuarios que no están ya en PostgreSQL)
    for (const mariaDBUser of mariaDBUsers) {
      // Obtener zone_id de la tabla zones
      const { rows: zoneRows } = await postgreSQLClient.query('SELECT id FROM zones WHERE code = $1', [mariaDBUser.zona]);
      let zoneId;
      if (zoneRows.length === 0) {
        // Insertar nueva zona si no existe
        const { rows: newZone } = await postgreSQLClient.query(
          'INSERT INTO zones (code, title, created_at, updated_at) VALUES ($1, $2, NOW(), NOW()) RETURNING id',
          [mariaDBUser.zona, mariaDBUser.zona]
        );
        zoneId = newZone[0].id;
        console.log(`Zona ${mariaDBUser.zona} insertada en PostgreSQL con ID ${zoneId}`);
      } else {
        zoneId = zoneRows[0].id;
      }

      // Obtener region_id de la tabla regions
      const { rows: regionRows } = await postgreSQLClient.query('SELECT id FROM regions WHERE code = $1', [mariaDBUser.region]);
      let regionId;
      if (regionRows.length === 0) {
        // Insertar nueva región si no existe
        const { rows: newRegion } = await postgreSQLClient.query(
          'INSERT INTO regions (code, title, created_at, updated_at) VALUES ($1, $2, NOW(), NOW()) RETURNING id',
          [mariaDBUser.region, mariaDBUser.region]
        );
        regionId = newRegion[0].id;
        console.log(`Región ${mariaDBUser.region} insertada en PostgreSQL con ID ${regionId}`);
      } else {
        regionId = regionRows[0].id;
      }

      // Obtener division_id de la tabla divisions
      const { rows: divisionRows } = await postgreSQLClient.query('SELECT id FROM divisions WHERE code = $1', [mariaDBUser.division]);
      let divisionId;
      if (divisionRows.length === 0) {
        // Insertar nueva división si no existe
        const { rows: newDivision } = await postgreSQLClient.query(
          'INSERT INTO divisions (code, title, created_at, updated_at) VALUES ($1, $2, NOW(), NOW()) RETURNING id',
          [mariaDBUser.division, mariaDBUser.division]
        );
        divisionId = newDivision[0].id;
        console.log(`División ${mariaDBUser.division} insertada en PostgreSQL con ID ${divisionId}`);
      } else {
        divisionId = divisionRows[0].id;
      }

      // Obtener section_id de la tabla sections
      const { rows: sectionRows } = await postgreSQLClient.query('SELECT id FROM sections WHERE code = $1', [mariaDBUser.seccion]);
      let sectionId;
      if (sectionRows.length === 0) {
        // Insertar nueva sección si no existe
        const { rows: newSection } = await postgreSQLClient.query(
          'INSERT INTO sections (code, title, created_at, updated_at) VALUES ($1, $2, NOW(), NOW()) RETURNING id',
          [mariaDBUser.seccion, mariaDBUser.seccion]
        );
        sectionId = newSection[0].id;
        console.log(`Sección ${mariaDBUser.seccion} insertada en PostgreSQL con ID ${sectionId}`);
      } else {
        sectionId = sectionRows[0].id;
      }

      const userKey = mariaDBUser.cuenta.toString();
      if (existingUsersMap.has(userKey)) {
        console.log(`Usuario con cuenta ${mariaDBUser.cuenta} ya existe en PostgreSQL. Se reemplaza.`);
        try {
          await postgreSQLClient.query(
            `
              UPDATE users SET username = $1, 
                               name = $2, 
                               email = $3, 
                               dni = $4, 
                               region_id = $5, 
                               zone_id = $6, 
                               section_id = $7, 
                               division_id = $8, 
                               password = $9,
                               updated_at = NOW()
              WHERE reseller_number = $10
            `,
            [
              mariaDBUser.dni.toString(),
              mariaDBUser.nombre,
              mariaDBUser.email,
              mariaDBUser.dni.toString(),
              regionId,
              zoneId,
              sectionId,
              divisionId,
              '',
              mariaDBUser.cuenta.toString()
            ]
          );
        } catch (updateError) {
          console.error(`Error al actualizar usuario ${mariaDBUser.nombre}:`, updateError);
        }
      } else {
        try {
          await postgreSQLClient.query(
            `INSERT INTO users (username, reseller_number, password, name, email, dni, region_id, zone_id, section_id, division_id, role_id, created_at, updated_at)
             VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, NOW(), NOW())`,
            [
              mariaDBUser.nombre,
              mariaDBUser.cuenta.toString(),
              'password_temporal',
              mariaDBUser.nombre,
              mariaDBUser.email,
              mariaDBUser.dni.toString(),
              regionId,
              zoneId,
              sectionId,
              divisionId,
              mariaDBUser.role_id
            ]
          );
          console.log(`Usuario ${mariaDBUser.nombre} (Cuenta: ${mariaDBUser.cuenta}, Zona: ${mariaDBUser.zona}) insertado en PostgreSQL`);
        } catch (insertError) {
          console.error(`Error al insertar usuario ${mariaDBUser.nombre}:`, insertError);
          if (insertError.code === '23505') {
            console.log(`El usuario con cuenta ${mariaDBUser.cuenta} ya existe en PostgreSQL. Actualizando...`);
            try {
              await postgreSQLClient.query(
                `
                  UPDATE users SET username = $1, 
                                   name = $2, 
                                   email = $3, 
                                   dni = $4, 
                                   region_id = $5, 
                                   zone_id = $6, 
                                   section_id = $7, 
                                   division_id = $8, 
                                   password = $9,
                                   updated_at = NOW()
                  WHERE reseller_number = $10
                `,
                [
                  mariaDBUser.nombre,
                  mariaDBUser.nombre,
                  mariaDBUser.email,
                  mariaDBUser.dni.toString(),
                  regionId,
                  zoneId,
                  sectionId,
                  divisionId,
                  'password_temporal',
                  mariaDBUser.cuenta.toString()
                ]
              );
            } catch (updateError) {
              console.error(`Error al actualizar usuario ${mariaDBUser.nombre}:`, updateError);
            }
          }
        }
      }
    }

    await mariaDBConnection.end();
    await postgreSQLClient.release();
    await postgreSQLPool.end();
    console.log('Sincronización completa');
  } catch (error) {
    console.error('Error durante la sincronización:', error);
  }
}


  
  // Opción para cerrar el pool cuando se detenga el proceso
  process.on('SIGINT', async () => {
    await postgreSQLPool.end();
    console.log('Conexiones cerradas. El proceso ha finalizado.');
    process.exit();
  });


module.exports = { sincronizarUsuarios };