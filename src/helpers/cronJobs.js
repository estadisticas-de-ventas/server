const cron = require('node-cron');
const { updateOrCreate } = require('./syncZoneSec');
// const { sincronizarUsuarios } = require('./syncUser');

// Crear el cron job para ejecutarse cada 1 hora
cron.schedule('0 */1 * * *', async () => {
  console.log('Ejecutando la función updateOrCreate...');
  await updateOrCreate();
 // console.log('Ejecutando la función sincronizarUsuarios...');
 // await sincronizarUsuarios();
});

// Opción para cerrar el pool cuando se detenga el proceso
process.on('SIGINT', async () => {
  await pgPool.end();
  console.log('Conexiones cerradas. El proceso ha finalizado.');
  process.exit();
});