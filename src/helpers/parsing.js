import { doesNotExist, exists } from './assertion';

/**
 * Converts string to snake_case
 *
 * @param {String} str          - String to be converted
 * @returns {String}            - Converted string
 */
export const camelToSnakeCase = str =>
  exists(str) ? str.replace(/[A-Z]/g, letter => `_${letter.toLowerCase()}`) : null;

/**
 * Get connection string from parameters
 *
 * @param {String} user                 - Database username
 * @param {String} pass                 - Database password
 * @param {String} host                 - Database host
 * @param {Number} port                 - Database port
 * @param {String} db                   - Database name
 * @param {String} engine               - Engine to use
 * @returns {string}                    - Return connection string
 */
export const getConnectionString = (user, pass, host, port, db, engine = 'postgres') =>
  `${engine}://${user}:${pass}@${host}:${port}/${db}`;

export const titleCase = str => {
  if (doesNotExist(str)) return str;
  const splitStr = str.toLowerCase().split(' ');
  for (let i = 0; i < splitStr.length; i++) {
    // You do not need to check if i is larger than splitStr length, as your for does that for you
    // Assign it back to the array
    splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  // Directly return the joined string
  return splitStr.join(' ');
};

export const chunks = (array, size) =>
  Array.apply(0, { length: Math.ceil(array.length / size) }).map((_, index) =>
    array.slice(index * size, (index + 1) * size)
  );

/**
 * Return day and month values from generic request
 *
 * @param {String} date                     - Date to be parsed
 * @return {{date: *, month: (*|string)}}
 */
export const getDayMonthFromDateRequest = date => ({
  month: date.substring(5, 7),
  day: date.substring(8, 10)
});

/**
 * Return concatenated day and month
 *
 * @param {String} date
 * @return {string}
 */
export const getDayMonthString = date => {
  const { month, day } = getDayMonthFromDateRequest(date);
  return `${month}${day}`;
};
