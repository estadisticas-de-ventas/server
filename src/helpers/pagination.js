import { anyDoNotExist } from './assertion';

/**
 * @param {RequestOptionsQueries}   queryOptions  - Query options obtained from request
 * @param {Object}                  res           - Express response object
 * @param {Number}                  totalRecords  - Total records
 * @param {String}                  resource      - Resource to include in header
 *
 * @return {Object}                 res           - Response object
 */
export const setContentRangeHeader = (queryOptions, res, totalRecords, resource = 'orders') => {
  if (anyDoNotExist(queryOptions, totalRecords, queryOptions.range)) {
    return res;
  }
  let rangeFrom = 0;
  let rangeTo = totalRecords;
  if (queryOptions.range.length === 2) {
    [rangeFrom, rangeTo] = queryOptions.range;
    if (rangeTo > totalRecords) {
      rangeTo = totalRecords;
    }
  }
  res.set('Content-Range', `${resource} ${rangeFrom}-${rangeTo}/${totalRecords}`);
};
