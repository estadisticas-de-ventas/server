import config from 'config';
import { sign, verify } from 'jsonwebtoken';
import crypto from 'crypto';

/**
 * Encrypt text through crypto library
 * @param  {string} text - Text to encrypt
 * @return {string}      - Encrypted text
 */
export const encrypt = text => {
  const cipher = crypto.createCipher(config.crypto.algorithm, config.crypto.password);
  let crypted = cipher.update(text, 'utf8', 'hex');
  crypted += cipher.final('hex');
  return crypted;
};

export const createToken = properties => {
  const toSign = {};
  Object.keys(properties).forEach(key => {
    if (key !== 'exp' && key !== 'iat') toSign[key] = properties[key];
  });
  return sign(toSign, config.secretKey, {
    //expiresIn: 365d
  });
};

export const readToken = token =>
  new Promise((resolve, reject) => {
    verify(token, config.secretKey, (err, decoded) => {
      if (err) return reject(err);
      return resolve(decoded);
    });
  });
