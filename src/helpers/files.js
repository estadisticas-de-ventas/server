import path from 'path';
import fs from 'fs';
import config from 'config';

export const renameOrderFiles = (imported, failed,
  importedPrefix = config.orderImports.prefixes.imported,
  failedPrefix = config.orderImports.prefixes.errored,
  doneDirectory = config.orderImports.doneDirectory.done,
  failedDirectory = config.orderImports.doneDirectory.errors) => {
  imported.forEach(f => {
    const filename = path.basename(f);
    fs.renameSync(f, `${doneDirectory}${importedPrefix}${filename}`);
  });
  failed.forEach(f => {
    const filename = path.basename(f);
    fs.renameSync(f, `${failedDirectory}${failedPrefix}${filename}`);
  });
};

export const setupImportsDirectoryTree = (done = config.orderImports.doneDirectory.done,
  errors = config.orderImports.doneDirectory.errors) => {
  if (!fs.existsSync(done)) {
    fs.mkdirSync(done);
  }
  if (!fs.existsSync(errors)) {
    fs.mkdirSync(errors);
  }
};

export const getOrdersFiles = (directory = config.orderImports.importsDirectory) => {
  const headerFilePaths = [];
  const detailFilePaths = [];
  const filePaths = fs.readdirSync(directory);

  filePaths.forEach(file => {
    if (
      file.substring(0, config.orderImports.prefixes.header.length)
        === config.orderImports.prefixes.header
    ) {
      return headerFilePaths.push(config.orderImports.importsDirectory + file);
    }
    if (
      file.substring(0, config.orderImports.prefixes.detail.length)
        === config.orderImports.prefixes.detail
    ) {
      return detailFilePaths.push(config.orderImports.importsDirectory + file);
    }
  });
  return { headerFilePaths, detailFilePaths };
};
