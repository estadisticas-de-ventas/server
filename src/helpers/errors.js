import { BAD_REQUEST } from 'http-status-codes';
import { exists } from './assertion';

export const setErrorHandlerMiddleware = env => setErrorHandlerMiddleware[env]
    // eslint-disable-next-line no-unused-vars
  || (setErrorHandlerMiddleware[env] = (err, req, res, next) => {
    const data = { message: err.message };
    if (['development', 'dev', 'test'].includes(env)) {
      data.stack = err.stack;
      data.error = err;
    }
    return res.status(exists(err.status) ? err.status : BAD_REQUEST).json(data);
  });
