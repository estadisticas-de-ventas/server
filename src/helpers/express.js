/**
 * Wraps middleware in promise in order to allow async/await usage
 *
 * @param {Function} fn                                 - Middleware function.
 * @return {function(*=, *=, *=): Promise<unknown>}    - Express middleware function.
 */
export const asyncMiddleware = fn => (req, res, next) => Promise.resolve(fn(req, res, next)).catch(next);
