import sequelize, { Op } from 'sequelize';
import pluralize from 'pluralize';
import { doesNotExist, exists } from './assertion';
import { camelToSnakeCase } from './parsing';
import { getModelNames } from '../models';

/**
 * @typedef {Object} RequestOptionsQueries
 * @property {Object} filter  - Property and value to filter by.
 * @property {Array}  range   - Range to .
 * @property {Array}  sort    - Sort by property and order.
 *
 * @param {Object}    req         - Http request
 * @return {RequestOptionsQueries}
 */
export const getFilterRangeSortQueries = req => ({
  filter: exists(req.query.filter) ? JSON.parse(req.query.filter) : null,
  range: exists(req.query.range) ? JSON.parse(req.query.range) : null,
  sort: exists(req.query.sort) ? JSON.parse(req.query.sort) : null
});

/**
 * Selects appropriate Sequelize
 *
 * @param {String|Number|Array} value         - Field value
 * @return {any}                              - Sequelize Op comparator
 */
export const selectSequelizeComparator = value =>
  Array.isArray(value)
    ? Op.in
    : typeof value === 'string' || isNaN(value) || value === ''
    ? Op.iLike
    : Op.eq;

/**
 * @typedef {Object} SequelizeOptions
 * @property {Object} filter  - The property and value to filter by.
 * @property {Number} offset  - Pagination offset.
 * @property {Number} limit   - Pagination limit.
 * @property {Array}  sort    - Property to sort by and order.
 *
 * @param {RequestOptionsQueries} options   - Options queries from request.
 *
 * @return {SequelizeOptions}               - Options formatted for direct Sequelize input.
 */
export const reqOptionsQueriesToSequelize = (options, idModel = null) => {
  const sequelizeOptions = {};
  if (exists(options.filter)) {
    sequelizeOptions.where = {};
    Object.keys(options.filter).forEach(key => {
      const snakeKey = camelToSnakeCase(key);
      const modelReference = getModelNames().some(
        model => snakeKey.includes(model) || snakeKey.includes(pluralize.singular(model))
      );

      if (!modelReference && (snakeKey.includes('date') || snakeKey.includes('at'))) {
        // TODO(ngoldenberg)[ucweb-appserver#1011]: Remove extra snake/camel case
        // constraints from filters after ucweb-appserver#1010 is dealt with
        let cleanKey = key;
        ['_from', '_to', 'from', 'to', 'From', 'To'].forEach(statement => {
          if (cleanKey.includes(statement)) {
            cleanKey = cleanKey.replace(statement, '');
          }
        });
        if (doesNotExist(sequelizeOptions.where[cleanKey])) {
          sequelizeOptions.where[cleanKey] = {};
        }
        if (snakeKey.includes('to')) {
          sequelizeOptions.where[cleanKey].$lte = options.filter[key];
        } else {
          sequelizeOptions.where[cleanKey].$gte = options.filter[key];
        }
        return;
      }
      // const operator = key === 'id' ? Op.eq : selectSequelizeComparator(options.filter[key]);
      const operator = selectSequelizeComparator(options.filter[key]);
      if (key === 'id' || key === 'q') {
        const value = key === 'id' ? `%${options.filter.id}%` : `%${options.filter.q}%`;
        const column = key === 'id' ? 'id' : 'code';
        key = sequelize.cast(sequelize.col(`${idModel}.${column}`), 'varchar');
        sequelizeOptions.where = sequelize.where(key, { [Op.iLike]: value });
      } else if (key === snakeKey) {
        sequelizeOptions.where[key] = {};
        sequelizeOptions.where[key][operator] =
          operator === Op.iLike ? `%${options.filter[key]}%` : options.filter[key];
      } else {
        // TODO(ngoldenberg)[ucweb-appserver#1011]: Remove extra snake/camel
        // case constraints from filters after ucweb-appserver#1010 is dealt with
        sequelizeOptions.where[key] = {};
        sequelizeOptions.where[key][operator] =
          operator === Op.iLike ? `%${options.filter[key]}%` : options.filter[key];
        // sequelizeOptions.where.$or = [{}, {}];
        // sequelizeOptions.where.$or[0][key] = {};
        // sequelizeOptions.where.$or[0][key][operator] =
        //   operator === Op.iLike ? `%${options.filter[key]}%` : options.filter[key];
        // sequelizeOptions.where.$or[1][snakeKey] = sequelizeOptions.where.$or[0][key];
      }
      // TODO: upgrade sequelize to v4.13.5 for better search like.
    });
  }
  if (exists(options.range)) {
    sequelizeOptions.offset = options.range[0];
    sequelizeOptions.limit = options.range[1] - options.range[0] + 1;
  }
  if (exists(options.sort)) {
    if (exists(options.sort.type) && options.sort.type === 'numericString') {
      sequelizeOptions.order = sequelize.literal(
        `SUBSTRING(${options.sort.field} FROM \'([0-9]+)\')::BIGINT ${options.sort.order},
        ${options.sort.field};`
      );
    } else if (Array.isArray(options.sort) && options.sort.length === 2) {
      sequelizeOptions.order = [options.sort];
    } else {
      sequelizeOptions.order = [[options.sort.field, options.sort.order]];
    }
  }
  return sequelizeOptions;
};

export const getTokenFromHeaderOrQuerystring = req => {
  if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
    return req.headers.authorization.split(' ')[1];
  }
  if (req.query && req.query.token) {
    return req.query.token;
  }
  return null;
};

// const redacted = '[REDACTED]';
// export const requestFilter = (req, propName) => {
//   if (propName === 'headers') {
//     return _.chain(req)
//       .get(propName)
//       .cloneDeep()
//       .assign(
//         _.pick(
//           {
//             authorization: redacted,
//             cookie: redacted
//           },
//           _.keys(req[propName])
//         )
//       )
//       .value();
//   }
//   return req[propName];
// };

export const requestFilter = (req, propName) => {
  if (propName === 'headers') {
    return Object.keys(req.headers).reduce((filteredHeaders, key) => {
      if (!['urbancanvastk', 'authorization'].includes(key)) {
        filteredHeaders[key] = req.headers[key];
      }
      return filteredHeaders;
    }, {});
  }
  return req[propName];
};
