import express from 'express';

import { list } from '../../controllers/orders';
import { verifyPermission } from '../../controllers/auth';
import { importOrders } from '../../controllers/importer';
import totals from './totals';

const routes = express.Router();

routes.get('/', verifyPermission('orders', 'read', 'all'), list);

routes.post('/import', verifyPermission('order', 'import', 'all'), importOrders);

routes.use('/totals', totals);

export default routes;
