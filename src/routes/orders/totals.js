import express from 'express';

import { get, list } from '../../controllers/totals';
import { verifyPermission } from '../../controllers/auth';

const routes = express.Router();

routes.get('/', verifyPermission('total', 'read', 'all'), list);

routes.get('/:campaignNumber', verifyPermission('total', 'read', 'all'), get);

export default routes;
