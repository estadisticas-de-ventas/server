import express from 'express';
var router = express.Router();

const TransformController = require('../controllers/transform');

router.get('/', TransformController.getTransformId);


module.exports = router;