import express from 'express';
import { OK } from 'http-status-codes';
import auth from './auth';
import users from './users';
import orders from './orders';
import imports from './imports';
import regions from './regions';
import sections from './sections';
import zones from './zones';
// import getDataDW from './dw';
import transformIdRouter from './transform';
import { setHeadersForCORS, sendNotFound } from '../helpers/response';
import db from '../models';
import { verifyPermission } from '../controllers/auth';

const routes = express.Router();

routes.use(setHeadersForCORS);

routes.use('/auth', auth);
routes.use('/users', users);
routes.use('/orders', orders);
routes.use('/imports', imports);
routes.use('/regions', regions);
routes.use('/sections', sections);
routes.use('/zones', zones);
// routes.use('/getDataDW', getDataDW);
routes.use('/getId', transformIdRouter);

routes.get('/', (req, res) => {
  res.status(OK).json({ message: 'Ok' });
});

// routes.get('/sync', verifyPermission('database', 'synchronize', 'all'), async (req, res) => {
routes.get('/sync', async (req, res) => {
  await db.sequelize.sync({ alter: true });
  res.status(OK).json({ message: 'Ok' });
});

routes.use((req, res) => {
  sendNotFound(res);
});

module.exports = routes;
