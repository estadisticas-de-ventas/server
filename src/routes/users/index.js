import express from 'express';

import { create } from '../../controllers/users';
import { importUsers } from '../../controllers/importer/users';

const routes = express.Router();

// TODO: Limit endpoint
routes.post('/', create);

routes.post('/import', importUsers);

export default routes;
