import express from 'express';
import {
  authenticate,
  editUserPassword,
  getPermissions,
  verifyPermission,
  checkAndNotifyUsers
} from '../../controllers/auth';
const checkAdmin = require('../../middlewares/verify')

const routes = express.Router();

routes.post('/login',[checkAdmin.checkUserAdmin], authenticate);
routes.post('/account', checkAndNotifyUsers);
routes.get('/permissions', verifyPermission('permissions', 'read', 'own'), getPermissions);
routes.put('/password', verifyPermission(), editUserPassword);

export default routes;
