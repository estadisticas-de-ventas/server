import express from 'express';
import { importDefaults } from '../../controllers/importer/default_entities';
import { verifyPermission } from '../../controllers/auth';
import { importUsers } from '../../controllers/importer/users';

const routes = express.Router();

routes.post('/defaults', importDefaults);

routes.post('/users', verifyPermission('users', 'import', 'all'), importUsers);

export default routes;
