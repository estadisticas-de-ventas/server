import express from 'express';
import createError from 'http-errors';
import { FAILED_DEPENDENCY } from 'http-status-codes';
import { list, get } from '../controllers/regions';
import { verifyPermission } from '../controllers/auth';
import { getFilterRangeSortQueries, reqOptionsQueriesToSequelize } from '../helpers/request';
import db from '../models';
import { setContentRangeHeader } from '../helpers/pagination';

const routes = express.Router();

routes.route('/', verifyPermission('region', 'read', 'all')).get(list);

routes.route('/:id', verifyPermission('region', 'read', 'all')).get(get);

export default routes;
