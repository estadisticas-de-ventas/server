import express from 'express';
var router = express.Router();

const DWController = require('../controllers/dw');

router.get('/', DWController.getDataUser);


module.exports = router;