import express from 'express';
import { list, get } from '../controllers/zones';
import { verifyPermission } from '../controllers/auth';

const routes = express.Router();

routes.get('/', verifyPermission('zone', 'read', 'all'), list);
routes.get('/:id', verifyPermission('zone', 'read', 'all'), get);

export default routes;
