import express from 'express';
import { list, get } from '../controllers/sections';
import { verifyPermission } from '../controllers/auth';

const routes = express.Router();

routes.route('/', verifyPermission('section', 'read', 'all')).get(list);

routes.route('/:id', verifyPermission('section', 'read', 'all')).get(get);

export default routes;
