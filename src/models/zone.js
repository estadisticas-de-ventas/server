import { Model } from 'sequelize';

const tableName = 'zones';

class Zone extends Model {
  static init(sequelize, DataTypes) {
    const options = { sequelize, tableName };
    return super.init(
      {
        code: {
          type: DataTypes.TEXT,
          allowNull: false
        },
        title: {
          type: DataTypes.TEXT,
          allowNull: false
        },
        createdAt: {
          field: 'created_at',
          type: DataTypes.DATE,
          allowNull: false
        },
        updatedAt: {
          field: 'updated_at',
          type: DataTypes.DATE,
          allowNull: false
        }
      },
      options
    );
  }

  static associate(models) {
    models.Zone.hasMany(models.OrderHead, {
      foreignKey: 'zone_id'
    });
    // models.Zone.hasMany(models.User);
    models.Zone.belongsToMany(models.Section, { through: 'zone_sections' });
    models.Zone.belongsTo(models.Division, {
      foreignKey: 'division_id',
      as: 'division'
    });
  }

  static rawRowToModel(row) {
    const [code, title] = row;
    return {
      code,
      title
    };
  }
}

export default Zone;
