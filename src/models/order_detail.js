import { Model } from 'sequelize';
import { exists } from '../helpers/assertion';

const tableName = 'order_details';

class OrderDetail extends Model {
  static init(sequelize, DataTypes) {
    const options = {
      sequelize,
      tableName,
      indexes: [
        {
          unique: true,
          fields: ['reseller_number', 'campaign_year', 'campaign']
        }
      ]
    };
    return super.init(
      {
        orderHeadId: {
          field: 'order_head_id',
          type: DataTypes.INTEGER,
          allowNull: false
        },
        resellerNumber: {
          field: 'reseller_number',
          type: DataTypes.STRING,
          allowNull: false
        },
        campaignYear: {
          field: 'campaign_year',
          type: DataTypes.INTEGER,
          allowNull: false
        },
        campaign: {
          type: DataTypes.INTEGER,
          allowNull: false
        },
        uniqueCode: {
          field: 'unique_code',
          type: DataTypes.STRING,
          unique: true
        },
        cosmetics: {
          type: DataTypes.INTEGER,
          allowNull: false,
          defaultValue: 0
        },
        extraCosmetics: {
          field: 'extra_cosmetics',
          type: DataTypes.INTEGER,
          allowNull: false,
          defaultValue: 0
        },
        currentDebt: {
          field: 'current_debt',
          type: DataTypes.INTEGER,
          allowNull: false,
          defaultValue: 0
        },
        payment: {
          type: DataTypes.STRING
        },
        repayment: {
          type: DataTypes.STRING
        },
        dismantling: {
          type: DataTypes.STRING
        },
        indicated: {
          type: DataTypes.STRING
        },
        deposit: {
          type: DataTypes.STRING
        },
        debtYear: {
          field: 'debt_year',
          type: DataTypes.STRING
        },
        debtCampaignNumber: {
          field: 'debt_campaign_number',
          type: DataTypes.STRING
        },
        preInvoice: {
          field: 'pre_invoice',
          type: DataTypes.STRING
        },
        ticketsEcommerce: {
          field: 'tickets_ecommerce',
          type: DataTypes.INTEGER,
          allowNull: false,
          defaultValue: 0
        },
        createdAt: {
          field: 'created_at',
          type: DataTypes.DATE,
          allowNull: false
        },
        updatedAt: {
          field: 'updated_at',
          type: DataTypes.DATE,
          allowNull: false
        }
      },
      options
    );
  }

  static rawRowToModel(row) {
    const [
      resellerNumber,
      campaignYear,
      campaign,
      cosmetics,
      extraCosmetics,
      currentDebt,
      payment,
      repayment,
      dismantling,
      indicated,
      debtYear,
      debtCampaignNumber,
      preInvoice,
      deposit,
      ticketsEcommerce
    ] = row;
    return {
      resellerNumber,
      campaignYear,
      campaign,
      uniqueCode: `${resellerNumber}${campaignYear}${campaign}`,
      cosmetics: exists(cosmetics) && cosmetics !== '' ? parseInt(cosmetics, 10) : 0,
      extraCosmetics:
        exists(extraCosmetics) && extraCosmetics !== '' ? parseInt(extraCosmetics, 10) : 0,
      currentDebt: exists(currentDebt) && currentDebt !== '' ? parseInt(currentDebt, 10) : 0,
      payment,
      repayment,
      dismantling,
      indicated,
      debtYear,
      debtCampaignNumber,
      preInvoice,
      deposit: deposit || '',
      ticketsEcommerce: parseInt(ticketsEcommerce, 10)
    };
  }
}

//TODO: jhuck en este archivo es donde se tiene que agregar el nuevo dato

export default OrderDetail;
