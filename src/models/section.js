import { Model } from 'sequelize';

const tableName = 'sections';

class Section extends Model {
  static init(sequelize, DataTypes) {
    const options = { sequelize, tableName };
    return super.init(
      {
        code: {
          type: DataTypes.TEXT,
          allowNull: false
        },
        title: {
          type: DataTypes.TEXT,
          allowNull: false
        },
        createdAt: {
          field: 'created_at',
          type: DataTypes.DATE,
          allowNull: false
        },
        updatedAt: {
          field: 'updated_at',
          type: DataTypes.DATE,
          allowNull: false
        }
      },
      options
    );
  }

  static associate(models) {
    models.Section.belongsToMany(models.Zone, { through: 'zone_sections' });
    models.Section.hasMany(models.OrderHead, {
      sourceKey: 'id',
      foreignKey: 'section_id'
    });
    // models.Section.hasMany(models.User);
  }

  static rawRowToModel(row) {
    const [code, title] = row;
    return {
      code,
      title
    };
  }
}

export default Section;
