import { Model } from 'sequelize';
import moment from 'moment';
import { titleCase } from '../helpers/parsing';
import { exists } from '../helpers/assertion';

const tableName = 'order_heads';

class OrderHead extends Model {
  static init(sequelize, DataTypes) {
    const options = {
      sequelize,
      tableName,
      indexes: [
        {
          unique: true,
          fields: ['reseller_number', 'campaign_year', 'campaign']
        },
        {
          unique: false,
          fields: ['zone_code', { order: 'DESC' }]
        },
        {
          unique: false,
          fields: ['section_code', { order: 'DESC' }]
        },
        {
          unique: false,
          fields: ['name', { order: 'DESC' }]
        },
        {
          unique: false,
          fields: ['address', { order: 'DESC' }]
        },
        {
          unique: false,
          fields: ['city', { order: 'DESC' }]
        },
        {
          unique: false,
          fields: ['province', { order: 'DESC' }]
        },
        {
          unique: false,
          fields: ['phone', { order: 'DESC' }]
        },
        {
          unique: false,
          fields: ['phone2', { order: 'DESC' }]
        },
        {
          unique: false,
          fields: ['date_of_birth', { order: 'DESC' }]
        },
        {
          unique: false,
          fields: ['birthday', { order: 'DESC' }]
        },
        {
          unique: false,
          fields: ['campaign_debt', { order: 'DESC' }]
        },
        {
          unique: false,
          fields: ['campaign_year_debt', { order: 'DESC' }]
        }
      ]
    };
    return super.init(
      {
        zoneCode: {
          field: 'zone_code',
          type: DataTypes.STRING,
          allowNull: false
        },
        sectionCode: {
          field: 'section_code',
          type: DataTypes.STRING,
          allowNull: false
        },
        resellerNumber: {
          field: 'reseller_number',
          type: DataTypes.STRING,
          unique: true
        },
        dni: {
          type: DataTypes.STRING
        },
        name: {
          type: DataTypes.STRING
        },
        address: {
          type: DataTypes.STRING
        },
        city: {
          type: DataTypes.STRING
        },
        provinceId: {
          field: 'province_id',
          type: DataTypes.INTEGER
        },
        province: {
          type: DataTypes.STRING
        },
        phone: {
          type: DataTypes.STRING
        },
        phone2: {
          type: DataTypes.STRING
        },
        dateOfBirth: {
          field: 'date_of_birth',
          type: DataTypes.DATE
        },
        birthday: {
          field: 'birthday',
          type: DataTypes.STRING
        },
        campaignDebt: {
          field: 'campaign_debt',
          type: DataTypes.INTEGER
        },
        campaignYearDebt: {
          field: 'campaign_year_debt',
          type: DataTypes.STRING
        },
        createdAt: {
          field: 'created_at',
          type: DataTypes.DATE,
          allowNull: false
        },
        updatedAt: {
          field: 'updated_at',
          type: DataTypes.DATE,
          allowNull: false
        },
        zoneid: {
          field: 'zone_id',
          type: DataTypes.INTEGER
        },        
        sectionid: {
          field: 'section_id',
          type: DataTypes.INTEGER
        }
      },
      options
    );
  }

  static associate(models) {
    models.OrderHead.hasMany(models.OrderDetail, {
      sourceKey: 'id',
      foreignKey: 'order_head_id'
    });
    models.OrderHead.belongsTo(models.Zone);
    models.OrderHead.belongsTo(models.Section);
  }

  static rawRowToModel(row) {
    const [ 
      zone,
      section,
      resellerNumber,
      name,
      address,
      city,
      provinceId,
      province,
      phone,
      phone2,
      dateOfBirth,
      campaignDebt,
      campaignYearDebt,
      dni,
      zoneid,
      sectionid
    ] = row;
    const parsedDateOfBirth =
      exists(dateOfBirth) && dateOfBirth !== '00.00.0000'
        ? moment(dateOfBirth, 'DD.MM.YYYY').format('YYYY-MM-DD HH:mm:ss')
        : null;

    let birthday = null;
    if (exists(parsedDateOfBirth)) {
      const month = parsedDateOfBirth.substring(5, 7);
      const day = parsedDateOfBirth.substring(8, 10);
      birthday = `${month}${day}`;
    }

    return {
      zoneCode: zone,
      sectionCode: section,
      resellerNumber,
      name: titleCase(name),
      address,
      city,
      provinceId,
      province,
      phone,
      phone2,
      dateOfBirth: parsedDateOfBirth,
      birthday,
      campaignDebt: exists(campaignDebt) ? parseInt(campaignDebt) : 0,
      campaignYearDebt,
      dni,
      zoneid,
      sectionid
    };
  }
}

export default OrderHead;
