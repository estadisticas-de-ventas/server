import { Model } from 'sequelize';

const tableName = 'users';

class User extends Model {
  static init(sequelize, DataTypes) {
    const options = { sequelize, tableName };
    return super.init(
      {
        username: {
          type: DataTypes.TEXT,
          allowNull: false
        },
        resellerNumber: {
          field: 'reseller_number',
          type: DataTypes.TEXT,
          allowNull: false,
          unique: true
        },
        password: {
          type: DataTypes.TEXT,
          allowNull: false
        },
        name: {
          type: DataTypes.TEXT
        },
        email: {
          type: DataTypes.TEXT
        },
        dni: {
          type: DataTypes.TEXT
        },
        regionId: {
          field: 'region_id',
          type: DataTypes.INTEGER,
          allowNull: true
        },
        zoneId: {
          field: 'zone_id',
          type: DataTypes.INTEGER,
          allowNull: true
        },
        sectionId: {
          field: 'section_id',
          type: DataTypes.INTEGER,
          allowNull: true
        },
        divisionId: {
          field: 'division_id',
          type: DataTypes.INTEGER,
          allowNull: true
        },
        roleId: {
          field: 'role_id',
          type: DataTypes.INTEGER,
          allowNull: false
        },
        createdAt: {
          field: 'created_at',
          type: DataTypes.DATE,
          allowNull: false
        },
        updatedAt: {
          field: 'updated_at',
          type: DataTypes.DATE,
          allowNull: false
        }
      },
      options
    );
  }

  static associate(models) {
    models.User.belongsTo(models.Zone, {
      foreignKey: 'zone_id',
      as: 'zone'
    });
    models.User.belongsTo(models.Section, {
      foreignKey: 'section_id',
      as: 'section'
    });
    models.User.belongsTo(models.Division, {
      foreignKey: 'division_id',
      as: 'division'
    });
    models.User.belongsTo(models.Region, {
      foreignKey: 'region_id',
      as: 'region'
    });
    models.User.belongsTo(models.Role, {
      foreignKey: 'role_id',
      as: 'role'
    });
  }

  static rawRowToModel(row) {
    const [username, password, name, email] = row;
    return {
      username,
      password,
      name,
      email
    };
  }
}

export default User;
