import { Sequelize } from 'sequelize';
import config from 'config';
import { getConnectionString } from '../helpers/parsing';
import OrderHead from './order_head';
import OrderDetail from './order_detail';
import User from './user';
import Division from './division';
import Region from './region';
import Role from './role';
import Section from './section';
import Zone from './zone';

/**
 * Get default Sequelize connection
 */
const getDefaultConnection = () =>
  new Sequelize(
    getConnectionString(
      config.database.username,
      config.database.password,
      config.database.host,
      config.database.port,
      config.database.database
    ),
    {
      logging: false,
      // logging: function (str) {
      //     console.log('SQL:',str)
      // },
      pool: {
        max: 500,
        min: 5,
        idle: 20000,
        evict: 15000,
        acquire: 30000
      }
    }
  );

const sequelize = getDefaultConnection();

const models = {
  Division: Division.init(sequelize, Sequelize),
  OrderDetail: OrderDetail.init(sequelize, Sequelize),
  OrderHead: OrderHead.init(sequelize, Sequelize),
  Region: Region.init(sequelize, Sequelize),
  Role: Role.init(sequelize, Sequelize),
  Section: Section.init(sequelize, Sequelize),
  User: User.init(sequelize, Sequelize),
  Zone: Zone.init(sequelize, Sequelize)
};

// Run `.associate` if it exists,
// ie create relationships in the ORM
Object.values(models)
  .filter(model => typeof model.associate === 'function')
  .forEach(model => model.associate(models));

const db = {
  ...models,
  sequelize,
  Sequelize
};

export const getModelNames = () => Object.keys(models);

export default db;
