import { Model } from 'sequelize';

const tableName = 'divisions';

class Division extends Model {
  static init(sequelize, DataTypes) {
    const options = { sequelize, tableName };
    return super.init(
      {
        code: {
          type: DataTypes.TEXT,
          allowNull: false
        },
        title: {
          type: DataTypes.TEXT,
          allowNull: false
        },
        createdAt: {
          field: 'created_at',
          type: DataTypes.DATE,
          allowNull: false
        },
        updatedAt: {
          field: 'updated_at',
          type: DataTypes.DATE,
          allowNull: false
        }
      },
      options
    );
  }

  static associate(models) {
    models.Division.hasMany(models.Zone, {
      foreignKey: 'division_id'
    });
    models.Division.belongsTo(models.Region);
  }

  static rawRowToModel(row) {
    const [code, title] = row;
    return {
      code,
      title
    };
  }
}

export default Division;
