
const axios = require('axios');
const { BASE_PATH, API_VERSION } = require('../utils/constants');
import db from '../models';
import { encrypt } from '../helpers/auth';
import createError from 'http-errors';
import { UNAUTHORIZED } from 'http-status-codes';

const checkUserAdmin = async (req, res, next) => {

    const {
    body: { username, password }
  } = req;

    const url = `${BASE_PATH}/${API_VERSION}/login`;

    axios
       .post(url, {
          username: username, password: password, origin: "edv"
       })
       .then(res => {

         console.log('Datawarehouse');
  
        if(res.status === 200) {
         return next();
        }
       })
       .catch(error => {

         console.log('EDV antigua');

          db.User.findAll({
            
            where: {
              resellerNumber: username,
              password: encrypt(password)
            }
          }).then( rows => {
            if (rows.length !== 0 ) {
              if (rows[0].dataValues.roleId == 8 || rows[0].dataValues.roleId == 7) {
                return next(); 
              }else{
                return next(
                   createError(UNAUTHORIZED, {
                     message: 'Authentication Error: Rol Unauthorized'
                   })
                 );
               }
              }else{
               return next(
                  createError(UNAUTHORIZED, {
                    message: 'Authentication Error: Unauthorized'
                  })
                );
              }
          })
      });
}

module.exports = {
    checkUserAdmin 
 };