export const defaultRegions = [
  { code: '01', title: 'Fuego' },
  { code: '02', title: 'Aire' },
  { code: '03', title: 'Gerencia Nacional' }
];

export const defaultDivisions = [
  {
    code: '101',
    title: 'Minerva'
  },
  {
    code: '102',
    title: 'Casandra'
  },
  {
    code: '103',
    title: 'Atenea'
  },
  {
    code: '104',
    title: 'Penélope'
  },
  {
    code: '105',
    title: 'Selene'
  },
  {
    code: '106',
    title: 'Helena'
  },
  {
    code: '107',
    title: 'Afrodita'
  },
  {
    code: '108',
    title: 'Gea'
  },
  {
    code: '109',
    title: 'Gorrión'
  },
  {
    code: '201',
    title: 'Venta Personal'
  }
];

export const defaultRoles = [
  {
    code: 'ZDGN',
    title: 'Gerente Nacional'
  },
  {
    code: 'ZDJV',
    title: 'Gerente Regional'
  },
  {
    code: 'ZDGE',
    title: 'Gerente Divisonal'
  },
  {
    code: 'ZDPR',
    title: 'Promotora'
  },
  {
    code: 'ZDLI',
    title: 'Lider'
  },
  {
    code: 'ZDRE',
    title: 'Revendedora'
  },
  {
    code: 'ZDAS',
    title: 'Asistente de Campo'
  },
  {
    code: 'ZDCO',
    title: 'Coach'
  }
];
