import db from '../models';
import { doesNotExist } from '../helpers/assertion';

export const getPreviousCampaign = async (campaign, campaignYear, modifier = 1) => {
  if (campaign > modifier) {
    return { campaign: campaign - modifier, campaignYear };
  }
  const campaignList = await db.OrderDetail.findAll({
    where: {
      campaignYear: campaignYear - 1
    },
    group: ['campaign', 'campaign_year'],
    order: [
      ['campaign_year', 'DESC'],
      ['campaign', 'DESC']
    ],
    attributes: ['campaign', 'campaign_year']
  });
  if (doesNotExist(campaignList[modifier - 1])) {
    throw new Error('Previous campaign does not exist');
  }
  return { campaign: campaignList[modifier - 1], campaignYear: campaignYear - 1 };
};

export const getCampaigns = async (sortOrder = 'ASC') => {
  const campaignList = await db.OrderDetail.findAll({
    group: ['campaign', 'campaign_year'],
    order: [
      ['campaign_year', sortOrder],
      ['campaign', sortOrder]
    ],
    attributes: ['campaign', 'campaignYear']
  });
  if (doesNotExist(campaignList)) {
    throw new Error('Campaign list could not be fetched');
  }

  if (campaignList.length === 0) {
    return [];
  }

  return campaignList.map(({ campaign, campaignYear }) => ({
    campaign,
    campaignYear,
    campaignNumber: `${campaignYear}${campaign.toString().padStart(2, '0')}`
  }));
};
