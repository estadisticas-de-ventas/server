import createError from 'http-errors';
import { FAILED_DEPENDENCY } from 'http-status-codes';
import { getFilterRangeSortQueries, reqOptionsQueriesToSequelize } from '../helpers/request';
import { setContentRangeHeader } from '../helpers/pagination';
import db from '../models';

export const list = async (req, res, next) => {
  const queryOptions = getFilterRangeSortQueries(req);
  const sequelizeOptions = reqOptionsQueriesToSequelize(queryOptions, 'Section');
  let records = [];
  let totalRecords = 0;
  const { Section, Zone } = db;
  try {
    const { count, rows } = await Section.findAndCountAll({
      ...sequelizeOptions,
      ...{ include: [{ model: Zone }] }
    });
    records = rows;
    totalRecords = count;
  } catch (error) {
    return next(
      createError(FAILED_DEPENDENCY, error, {
        message: 'List Section collection failed'
      })
    );
  }
  setContentRangeHeader(queryOptions, res, totalRecords);
  return res.json(records);
};

export const get = async (req, res, next) => {
  const { id } = req.params;
  const { Section, Zone } = db;
  try {
    const zone = await Section.findByPk(id, { include: [{ model: Zone }] });
    return res.json(zone);
  } catch (error) {
    return next(
      createError(FAILED_DEPENDENCY, error, { message: `Get Section '${id}' collection failed` })
    );
  }
};
