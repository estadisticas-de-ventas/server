import createError, { UnprocessableEntity } from 'http-errors';
import { FAILED_DEPENDENCY } from 'http-status-codes';
import { defaults } from 'config';
import db from '../models';
import { anyDoNotExist, exists } from '../helpers/assertion';
import { encrypt } from '../helpers/auth';

export const create = async (req, res, next) => {
  const { username, email, name, resellerNumber, dni } = req.body;
  if (anyDoNotExist(username, resellerNumber)) {
    return next(
      new UnprocessableEntity('At least one required parameter is missing: username or password')
    );
  }
  const password = exists(req.body.password) ? req.body.password : defaults.users.password;

  let foundUsers = await db.User.findAll({ where: { username }, include: [db.Role] });
  if (foundUsers.length > 0) {
    return next(new UnprocessableEntity('Username already taken, please choose another one'));
  }
  foundUsers = await db.User.findAll({ where: { resellerNumber }, include: [db.Role] });
  if (foundUsers.length > 0) {
    return next(
      new UnprocessableEntity('Reseller number already taken, please choose another one')
    );
  }

  const { roleId, sectionId, zoneId, divisionId, regionId } = req.body;

  const options = {
    password: encrypt(password),
    username,
    resellerNumber,
    email,
    name,
    dni,
    roleId,
    sectionId,
    zoneId,
    divisionId,
    regionId
  };
  let user;
  try {
    user = await db.User.create(options, { include: [db.Role] });
  } catch (error) {
    return next(
      createError(FAILED_DEPENDENCY, error, {
        message: 'List OrderHead collection failed'
      })
    );
  }
  const userData = {
    id: user.id,
    name: user.name,
    username: user.username,
    resellerNumber: user.resellerNumber
  };
  return res.json(userData);
};
