import createError from 'http-errors';
import { FAILED_DEPENDENCY } from 'http-status-codes';
import { Op } from 'sequelize';
import { getFilterRangeSortQueries, reqOptionsQueriesToSequelize } from '../helpers/request';
import { setContentRangeHeader } from '../helpers/pagination';
import db from '../models';

export const list = async (req, res, next) => {
  //console.log("jhuck ", req);
  //console.log("jhuck ", res);
  //console.log("jhuck ", next);

  const queryOptions = getFilterRangeSortQueries(req);
  const sequelizeOptions = reqOptionsQueriesToSequelize(queryOptions, 'Zone');

  const { code } = req.decoded.role;
  const { zoneId, divisionId, regionId } = req.decoded.user;

  // TODO: Unhardcode role codes
  // TODO: Fix camel case models
  if (code === 'ZDGE') {
    //console.log("jhuck ", divisionId, zoneId);

    // ! TODO: acá se filtran las zonas, revisar bien este punto para que no traiga los division_id en NULL
    sequelizeOptions.where = {
      [Op.or]: [{ division_id: divisionId }, { id: zoneId }]
    };
  } else if (code === 'ZDJV') {
    const divisions = await db.Division.findAll({ where: { region_id: regionId } });
    sequelizeOptions.where = {
      division_id: {
        [Op.in]: divisions.map(d => d.id)
      }
    };
  } else if (code === 'ZDPR') {
    sequelizeOptions.where = {
      id: zoneId
    };
  } else if (code === 'ZDCO') {
    sequelizeOptions.where = {
      id: zoneId
    };
  }
   /* TODO quitar else if(code !== 'ZDGN') {
    sequelizeOptions.where = {
      id: zoneId
    };
  } */

  let records = [];
  let totalRecords = 0;
  const { Zone, Section } = db;
  try {
    const { count, rows } = await Zone.findAndCountAll({
      ...sequelizeOptions,
      ...{ include: [{ model: Section, order: ['code', 'ASC'] }] }
    });
    records = rows;
    totalRecords = count;
  } catch (error) {
    return next(
      createError(FAILED_DEPENDENCY, error, {
        message: 'List Zone collection failed'
      })
    );
  }
  setContentRangeHeader(queryOptions, res, totalRecords);
  return res.json(records);
};

export const get = async (req, res, next) => {
  //console.log("jhuck ", req);
  //console.log("jhuck ", res);
  //console.log("jhuck ", next);

  const { id } = req.params;
  const { Zone, Section } = db;
  try {
    const zone = await Zone.findByPk(id, { include: [{ model: Section, order: ['code', 'ASC'] }] });
    return res.json(zone);
  } catch (error) {
    return next(
      createError(FAILED_DEPENDENCY, error, { message: `Get Zone '${id}' collection failed` })
    );
  }
};
