import createError from 'http-errors';
import { FAILED_DEPENDENCY } from 'http-status-codes';
import { Op } from 'sequelize';
import ora from 'ora';
import chalk from 'chalk';
import { getFilterRangeSortQueries, reqOptionsQueriesToSequelize } from '../helpers/request';
import { setContentRangeHeader } from '../helpers/pagination';
import db from '../models';
import { doesNotExist, exist, exists, isNotEmpty } from '../helpers/assertion';
import OrderDetail from '../models/order_detail';
import { getDayMonthFromDateRequest, getDayMonthString } from '../helpers/parsing';

export const customizeFilters = queryOptions => {
  const spinner = ora('').start();
  if (exist(queryOptions.filter) && exist(queryOptions.filter.q)) {
    const { q } = queryOptions.filter;
    queryOptions.filter.code = q;
    delete queryOptions.filter.q;
  }

  const preWhereObject = {};
  if (exists(queryOptions.filter)) {
    if (exists(queryOptions.filter.campaignDebtFrom)) {
      const { campaignDebtFrom } = queryOptions.filter;
      delete queryOptions.filter.campaignDebtFrom;
      preWhereObject.campaignDebtFrom = campaignDebtFrom;
    }
    if (exists(queryOptions.filter.campaignDebtTo)) {
      const { campaignDebtTo } = queryOptions.filter;
      delete queryOptions.filter.campaignDebtTo;
      preWhereObject.campaignDebtTo = campaignDebtTo;
    }
    if (exists(queryOptions.filter.resellerNumber)) {
      const { resellerNumber } = queryOptions.filter;
      queryOptions.filter.resellerNumber = resellerNumber.toString();
    }
    
  }
  const sequelizeOptions = reqOptionsQueriesToSequelize(queryOptions);

  let orderDetailsWhere = {};
  let orderDetailsRelation = {};
  if (doesNotExist(sequelizeOptions.where)) {
    sequelizeOptions.where = {};
  }

  if (exists(queryOptions.filter.currentDebt) && queryOptions.filter.currentDebt == true && (!exists(queryOptions.filter.campaignSelector))) {

    sequelizeOptions.where.campaign_debt = {
      [Op.gt]: 0
    };

  }

  if (exists(preWhereObject.birthdayFrom) && exists(preWhereObject.birthdayTo)) {
    sequelizeOptions.where = Object.assign(sequelizeOptions.where, {
      birthday: {
        [Op.between]: [preWhereObject.birthdayFrom, preWhereObject.birthdayTo]
      }
    });
  } else if (exists(preWhereObject.birthdayFrom)) {
    sequelizeOptions.where = Object.assign(sequelizeOptions.where, {
      birthday: {
        [Op.gte]: preWhereObject.birthdayFrom
      }
    });
  } else if (exists(preWhereObject.birthdayTo)) {
    sequelizeOptions.where = Object.assign(sequelizeOptions.where, {
      birthday: {
        [Op.lte]: parseInt(preWhereObject.birthdayTo)
      }
    });
  } else if (exists(preWhereObject.campaignDebtFrom) && exists(preWhereObject.campaignDebtTo)) {
    sequelizeOptions.where = Object.assign(sequelizeOptions.where, {
      campaignDebt: {
        [Op.between]: [preWhereObject.campaignDebtFrom, preWhereObject.campaignDebtTo]
      }
    });
  } else if (exists(preWhereObject.campaignDebtFrom)) {
    sequelizeOptions.where = Object.assign(sequelizeOptions.where, {
      campaignDebt: {
        [Op.gte]: parseInt(preWhereObject.campaignDebtFrom, 10)
      }
    });
  } else if (exists(preWhereObject.campaignDebtTo)) {
    sequelizeOptions.where = Object.assign(sequelizeOptions.where, {
      campaignDebt: {
        [Op.gt]: 0,
        [Op.lte]: parseInt(preWhereObject.campaignDebtTo, 10)
      }
    });
  } else if (
    exists(queryOptions.filter) &&
    (exists(queryOptions.filter.campaignSelector) ||
    Object.keys(queryOptions.filter).some(f =>
      ['hasDisarm', 'hasPayment', 'hasRepayment','deposit','campaignOrders'].includes(f)
    ))
  ) {
    const { campaignSelector, hasDisarm, hasPayment, hasRepayment,deposit,campaignOrders,currentDebt} = queryOptions.filter;
    // TODO: think about replacing this hardcoded model name
    
    if(exists(campaignSelector) && campaignSelector != false){
      orderDetailsWhere = {
        campaignYear: Number(campaignSelector.substring(0, 4)),
        campaign: Number(campaignSelector.substring(4))
      };
      
      delete sequelizeOptions.where.campaignSelector;
    }else if(campaignSelector === false){
      delete sequelizeOptions.where.campaignSelector;
    }

    if(exists(currentDebt) && currentDebt != false){
      orderDetailsWhere.currentDebt = {
        [Op.gt]: 0
      };
    }

    if (exists(hasDisarm)) {
      orderDetailsWhere.dismantling = hasDisarm ? 'D' : '';
      delete sequelizeOptions.where.hasDisarm;
    }


    if (exists(hasPayment)) {
      orderDetailsWhere.payment = hasPayment ? 'PC' : '';
      delete sequelizeOptions.where.hasPayment;
    }
    
    if (exists(deposit)) {
      orderDetailsWhere.deposit = { [Op.eq]: (deposit ? '*' : '') };
      delete sequelizeOptions.where.deposit;
    }
    if (hasRepayment) {
      orderDetailsWhere.repayment = hasRepayment ? '*' : '';
      delete sequelizeOptions.where.hasRepayment;
    }

    if (exists(campaignOrders)) {
      orderDetailsRelation.campaignOrders=campaignOrders;
    }
    delete sequelizeOptions.where.campaignOrders;

  }

  delete sequelizeOptions.where.currentDebt;

  spinner.info(`${chalk.cyan.bold('sequelizeOptions')}: \n ${JSON.stringify(sequelizeOptions)}`);
  // return;
  return { sequelizeOptions, orderDetailsWhere , orderDetailsRelation};
};

export const list = async (req, res, next) => {
  const queryOptions = getFilterRangeSortQueries(req);

  const temporarySequelizeQueryFix = {};
  if (exists(queryOptions.filter)) {
    if (exist(queryOptions.filter.dateOfBirthFrom, queryOptions.filter.dateOfBirthTo)) {
      const { dateOfBirthFrom, dateOfBirthTo } = queryOptions.filter;
      if (
        parseInt(getDayMonthString(dateOfBirthFrom), 10) >
        parseInt(getDayMonthString(dateOfBirthTo), 10)
      ) {
        temporarySequelizeQueryFix.birthday = {
          [Op.or]: [
            { [Op.between]: [getDayMonthString(dateOfBirthFrom), '1231'] },
            { [Op.between]: ['0101', getDayMonthString(dateOfBirthTo)] }
          ]
        };
      } else {
        temporarySequelizeQueryFix.birthday = {
          [Op.gte]: getDayMonthString(dateOfBirthFrom),
          [Op.lte]: getDayMonthString(dateOfBirthTo)
        };
      }
    } else if (exists(queryOptions.filter) && exists(queryOptions.filter.dateOfBirthTo)) {
      temporarySequelizeQueryFix.birthday = {
        [Op.lte]: getDayMonthString(queryOptions.filter.dateOfBirthTo)
      };
    } else if (exists(queryOptions.filter.dateOfBirthFrom)) {
      temporarySequelizeQueryFix.birthday = {
        [Op.gte]: getDayMonthString(queryOptions.filter.dateOfBirthFrom)
      };
    }
  }

  delete queryOptions.filter.dateOfBirthTo;
  delete queryOptions.filter.dateOfBirthFrom;
  delete queryOptions.filter.birthdayTo;
  delete queryOptions.filter.birthdayFrom;

  ora('smt').info(`${chalk.cyan('queryOptions')}: ${JSON.stringify(queryOptions)}`);

  const { sequelizeOptions, orderDetailsWhere ,orderDetailsRelation} = customizeFilters(queryOptions);
  let options = { include: [{ model: db.OrderDetail }] };
  
  if (isNotEmpty(orderDetailsWhere) && (!exists(orderDetailsRelation.campaignOrders) || orderDetailsRelation.campaignOrders == true)) {
    options = { include: [{ model: db.OrderDetail }] };
    if (doesNotExist(sequelizeOptions.where)) {
      sequelizeOptions.where = {};
    }
    sequelizeOptions.where.id = (await OrderDetail.findAll({ where: orderDetailsWhere , group: "orderHeadId",attributes:['orderHeadId']})).map(
      r => r.orderHeadId
    );
    
  }
   
  if(exists(orderDetailsRelation.campaignOrders) && !orderDetailsRelation.campaignOrders){

    if(exists(orderDetailsWhere.campaignYear) && exists(orderDetailsWhere.campaign) && !orderDetailsRelation.campaignOrders){

      if (doesNotExist(sequelizeOptions.where)) {
        sequelizeOptions.where = {};
      }
      let queryFilterNew =JSON.parse(req.query.filter);

      const TempHeadQuery={
          zone_id: queryFilterNew.zone_id
      };

      if(typeof queryFilterNew.section_id !== "undefined"){
        TempHeadQuery.section_id=queryFilterNew.section_id;
      }


      const tempIds = db.sequelize.dialect.queryGenerator.selectQuery('order_heads',{
        attributes: ['reseller_number'],
        where: TempHeadQuery})
      .slice(0,-1);

      
      let newWhereDetail = {
        campaignYear:orderDetailsWhere.campaignYear,
        campaign:orderDetailsWhere.campaign,
        resellerNumber:{
          [Op.in]:db.sequelize.literal(`(${tempIds})`)
        }
      };
      
      newWhereDetail = (await OrderDetail.findAll({ where: newWhereDetail , group: "orderHeadId",attributes:['orderHeadId']})).map(
        r => r.orderHeadId
      );
      
      newWhereDetail={
        orderHeadId:{
          [Op.notIn]:newWhereDetail
        }
      }
      

      sequelizeOptions.where.id = (await OrderDetail.findAll({ where: newWhereDetail , group: "orderHeadId",attributes:['orderHeadId']})).map(
        r => r.orderHeadId
      );
        




    }else{
      return next(

        createError(FAILED_DEPENDENCY, error, {
          message: 'You must select a campaign to use the filter for resellers not in the campaign.'
        })
      );
    }
    
  }
  
  sequelizeOptions.where = { ...sequelizeOptions.where, ...temporarySequelizeQueryFix };



  const resultingOptions = {
    ...sequelizeOptions,
    ...options
  };

  // Apply secondary sort
  if (doesNotExist(resultingOptions.order)) {
    resultingOptions.order = [];
  }
  if (
    resultingOptions.order.length > 0 &&
    !resultingOptions.order.some(([key]) => key === 'resellerNumber')
  ) {
    resultingOptions.order.push(['resellerNumber', 'ASC']);
  }

  resultingOptions.distinct = 'id';
  let records = [];
  let totalRecords = 0;
  const { OrderHead } = db;
 
  try {
    // const { count, rows } = await OrderHead.findAndCountAll(resultingOptions);
    const { count, rows } = await OrderHead.findAndCountAll(resultingOptions);
    records = rows;
    totalRecords = count;
  } catch (error) {
    return next(
      createError(FAILED_DEPENDENCY, error, {
        message: 'List OrderHead collection failed'
      })
    );
  }

  setContentRangeHeader(queryOptions, res, totalRecords);
  const jsonString = JSON.stringify(records);
  records = JSON.parse(jsonString);

  // TODO: move this
  records.forEach((record, index) => {
    if (exists(record.OrderDetails)) {
      const orderDetails = {};
      record.OrderDetails.forEach(
        d => (orderDetails[`${d.campaignYear}${d.campaign.toString().padStart(2, '0')}`] = d)
      );
      records[index].records = orderDetails;
      records[index].OrderDetails = undefined;
    }
  });
  return res.json(records);
};
