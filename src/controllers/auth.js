import createError, { UnprocessableEntity } from 'http-errors';
import { UNAUTHORIZED } from 'http-status-codes';
const { BASE_PATH, API_VERSION } = require('../utils/constants');
import { exists, anyDoNotExist, doesNotExist } from '../helpers/assertion';
import { getTokenFromHeaderOrQuerystring } from '../helpers/request';
import { createToken, encrypt, readToken } from '../helpers/auth';
import { sendUnauthorized } from '../helpers/response';
import db from '../models';
import { getCampaigns } from './campaigns';
import axios from 'axios';


export const authenticate = async (req, res, next) => {
  const {
    body: { username, password }
  } = req;
  if (anyDoNotExist(username, password)) {
    return next(
      new UnprocessableEntity('At least one required parameter is missing: username or password')
    );
  }
      
  const rows = await db.User.findAll({
    // include: [{ model: db.Role, raw: true }],
    //TODO: only reseller_number is needed, needs to be replaces by username and import
    where: {
      resellerNumber: username,
      password: encrypt(password)
    },
    attributes: { exclude: ['password'] },
    include: [
      { model: db.Role, as: 'role' },
      { model: db.Zone, as: 'zone' },
      { model: db.Section, as: 'section' },
      { model: db.Region, as: 'region' },
      { model: db.Division, as: 'division' }
    ]
  });

  // try {

  if (rows.length !== 1) {
    return next(
      createError(UNAUTHORIZED, {
        message: 'Authentication failed: could not find suitable User for credentials'
      })
    );
  }
  const [userData] = rows;

  const role = await userData.getRole();

  // JHUCK
  /*if( role.dataValues.id >= 5 ){
    //console.log("jhuck ", role.dataValues.id );
    return res.json({
      success: false,
      message: 'Usuario inhabilitado',
    });
  } */

  
  const token = createToken({ user: userData, role });
  return res.json({
    token,
    success: true,
    message: 'Token created',
    user: userData,
    role
  });
 }



// La función checkUserStatus recibe res para manejar la solicitud HTTPS correctamente
const checkUserStatus = async (cuenta, res) => {
  try {

    const validateStatusUser = `${BASE_PATH}/${API_VERSION}/edv/validate-status-user`;    

    const response = await axios.post(validateStatusUser, { 
      cuenta: cuenta
    }, {
      headers: {
        'ApiKey': '370408df-cb92-4421-8b07-fd3a0adf8317',
        'Content-Type': 'application/json'
      }
    });

    // Se agrega la cabecera CORS
    res.set('Access-Control-Allow-Origin', '*');  // Permite solicitudes desde cualquier dominio

    return response.data;
  } catch (error) {
    throw new Error(error.response?.data?.message || 'Error en la solicitud');
  }
};


// Lógica para verificar el estado del usuario
export const checkAndNotifyUsers = async (req, res, next) => {

  const { cuenta } = req.body;

  try {
    const response = await checkUserStatus(cuenta, res); // La respuesta de checkUserStatus es lo que se obtiene
    const status = response.result[0].estado; // Extraer el estado de la respuesta

    if (status === '04') {
      res.status(200).json({ estado: '04' });
    } else {
      res.status(200).json({ estado: 'Estado aceptado' });
    }
  } catch (error) {
    res.status(500).json({ estado: 'Usuario no válido', error: error.message });
  }
};


export const verifyPermission = (resource = null, action = null, ownership = null) =>
  verifyPermission[(resource, action, ownership)] ||
  (verifyPermission[(resource, action, ownership)] = async (req, res, next) => {
    const token = getTokenFromHeaderOrQuerystring(req);
    if (doesNotExist(token)) return sendUnauthorized(res, 'No token provided.');
    try {
      const decoded = await readToken(token);
      req.decoded = decoded;
      // if (await ucApi.hasAclPermission(resource, action, ownership)) {
      //   return next();
      // }
      // TODO: apply permissions check
      if (true) {
        return next();
      }
    } catch (error) {
      return next(
        createError(UNAUTHORIZED, error, {
          message: 'Unable to verify user permissions'
        })
      );
    }
    return next(
      sendUnauthorized(
        res,
        `User does not have access to resource (${resource}), action (${action}) or ownership(${ownership})`
      )
    );
  });

export const getPermissions = async (req, res, next) => {
  const { user, role } = req.decoded;
  const campaigns = await getCampaigns();
  return res.json({ campaigns, user, role: exists(role) ? role : null });
};

export const editUserPassword = async (req, res, next) => {
  const {
    body: { password }
  } = req;
  if (anyDoNotExist(password)) {
    return next(new UnprocessableEntity('One required parameter is missing: password'));
  }
  const { user } = req.decoded;
  await db.User.update(
    { password: encrypt(password) },
    {
      where: {
        id: user.id
      }
    }
  );
  return res.json({
    status: 'done',
    message: 'Password successfully changed',
    //password: encrypt(password)
  });
};
