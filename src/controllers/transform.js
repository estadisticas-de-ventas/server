import db from '../models';
const { Section, Zone, Division, Region, Role } = db;

const getTransformId = (req, res, next) => {
    const { rol, zona, seccion, division, region } = req.body;

    const loadDataPromises = [
    Role.findOne({ attributes:['id'], where: { 'code': rol } }),
    Zone.findOne({ attributes:['id'], where: { 'code': zona } }),   
    Division.findOne({ attributes:['id'], where: { 'code': division } }),
    Region.findOne({ attributes:['id'], where: { 'code': region } }),
    Section.findOne({ attributes:['id'], where: { 'code': seccion } })
    ]

    Promise.all(loadDataPromises)
    .then((results) => {
        var rol = results[0].dataValues.id;
        var zona = results[1].dataValues.id;
        var division = results[2].dataValues.id;
        var region = results[3].dataValues.id;
        var seccion = results[4].dataValues.id;
        
        res.status(200).json({ rol, zona, division, region, seccion });
    });
   }


module.exports = {
    getTransformId
}