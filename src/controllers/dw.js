
import db from '../models';
import Division from '../models/division';
const axios = require('axios')
const cron = require('node-cron');
require('dotenv').config();
var SMB2 = require('smb2');

const { Zone_sections, User, Zone } = db;

const getDataUser = async () => {

    var customerData = {
        method: 'get',
        url: process.env.URL_DATA_USER,
        headers: { 
          //'Apikey': process.env.API_KEY
        },
      };

axios(customerData)
.then(function (response) {
  
        if (response.data.result.length === 0) {
            recall();
        return false;
        }else{
            
            for (let i = 0; i < response.data.result.length; i++) {
                User.findOne({ where: { 'reseller_number': `${response.data.result[i].cuenta }` } }).then( result => {

                if (response.data.result[i].tipo_cuenta != 'ZDRE' && response.data.result[i].tipo_cuenta != 'ZDEM') {
                                            
                  var config = {
                    method: 'get',
                    url: process.env.URL_TRANSFORM_ID,
                    headers: {},
                    data : {
                      'rol': `${response.data.result[i].tipo_cuenta }`,
                      'zona': `${response.data.result[i].zona }`,
                      'region': `${response.data.result[i].region }`,
                      'seccion': `${response.data.result[i].seccion }`,
                      'division': `${response.data.result[i].division }`
                    }
                  };
                  

                   if (!result) {

                    axios(config)
                    .then(function (result) {
                      
                      User.create({
                        resellerNumber: `${response.data.result[i].cuenta}`,
                        name: `${response.data.result[i].nombre}`,
                        username: `${response.data.result[i].dni}`,
                        password: '',
                        email: `${response.data.result[i].email}`,
                        dni: `${response.data.result[i].dni}`,
                        roleId: result.data.rol,
                        zoneId: result.data.zona,
                        sectionId: result.data.seccion,
                        divisionId: result.data.division,
                        regionId: result.data.region
                    }).then( res => {}).catch(err => {console.log(err);})
                    }).catch(err => console.log(err));

                   }else{
                    axios(config)
                    .then(function (result) {

                      var values = {
                        resellerNumber: `${response.data.result[i].cuenta}`,
                        name: `${response.data.result[i].nombre}`,
                        username: `${response.data.result[i].dni}`,
                        password: '',
                        email: `${response.data.result[i].email}`,
                        dni: `${response.data.result[i].dni}`,
                        role_id: result.data.rol,
                        zone_id: result.data.zona,
                        section_id: result.data.seccion,
                        division_id: result.data.division,
                        region_id: result.data.region
                      }

                      var condition = { where: {
                        resellerNumber: `${response.data.result[i].cuenta}`
                      }}

                      let options = { multi: true };

                      User.update(values, condition, options, {
                       
                    }).then( res => {}).catch(err => {console.log(err);})
                    }).catch(err => console.log(err));
                   }
                }
               }).catch( err => {console.log(err);});
            }
        }
    }).catch( err => {console.log(err);});
}

const getSalesStructures = async () => {
  var orderData = new SMB2({
    // OUTERQ$ = QA
    // OUTEPR$ = PR
      share: '\\\\ms10004.vfc.com.ar\\OUTERQ$'
    , domain: 'vfc'
    , username: 'estadistica_service'
    , password: 'gtgx1$/}'
  });

  orderData.readdir('DataWarehouse\\ventas\\Procesar', function(err, files){
    if(err) throw err;
  
    files.forEach(function(file) {
      orderData.readFile('DataWarehouse\\ventas\\Procesar\\' + file, function(err, data){
        if (err) throw err;
        var contenido = data.toString('utf-8');
        var lineas = contenido.trim().split("\n");
        var objeto = {};
      
        lineas.forEach(function(linea, indice) {
          var valores = linea.trim().split("\t");
          for (var i = 0; i < valores.length; i++) {
            objeto["x" + (i+1)] = valores[i];
          }
    
          var config = {
            method: 'get',
            url: process.env.URL_SALES_STRUCTURES,
            headers: {},
            data : {
              'division': `${objeto.x1 }`,
              'region': `${objeto.x2 }`,
              'zona': `${objeto.x3 }`,
              'seccion': `${objeto.x4 }`,
            }
          };
  
          axios(config)
          .then(function (result) {
  
            let sales_structure = result.data.restructure;
            let options = { multi: true };
            let str = [];
  
            for (let i = 0; i < sales_structure.length; i++) {
              str.push(sales_structure[i].id);
            }
  
            var division = str[2];
            var region = str[0];
  
            var condition = { where: {
              id: str[1]
            }}
  
            var div = {
              division_id: `${division}`
            }
  
            Zone.update(div, condition, options,{
  
            }).then( res => {}).catch(err => {console.log(err);})
          
            var values = {
              section_id: `${str[3]}`,
              zone_id: `${str[1]}`
            }
  
            Zone_sections.create(values, options, {
                          
            }).then( res => {}).catch(err => {console.log(err);})
  
            var condition = { where: {
              id: str[2]
            }}
  
            var reg = {
              region_id: `${region}`
            }
  
            Division.update(reg, condition, options, {
                          
            }).then( res => {}).catch(err => {console.log(err);})
  
          }).catch(function (err)  {console.error(err);});
        });
      });
    });
  });
  
}


const recall = () => {
    getDataUser();
  }

cron.schedule(process.env.TIME_EXECUTION, () => {
    getDataUser();
    // getSalesStructures();
   
   }, {
     scheduled: true,
     timezone: "America/Argentina/Buenos_Aires"
   });

module.exports = {
    getDataUser
}