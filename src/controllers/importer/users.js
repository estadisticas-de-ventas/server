import { defaults } from 'config';
import createError from 'http-errors';
import { FAILED_DEPENDENCY } from 'http-status-codes';
import ora from 'ora';
import db from '../../models';
import { importFile } from './default_entities';
import { encrypt } from '../../helpers/auth';
import { doesNotExist, exists } from '../../helpers/assertion';

const cifrar = text => {
  let t = Array.from(String( parseInt( (parseInt(text) / 4) * 3) ), Number);

  t.splice(4, 1);
  t.splice(6, 1);

  return t.join('');
}

export const importUsers = async (req, res, next) => {
  const startText = 'Run import users job...';
  const spinner = ora(startText).start();
  // TODO: move path to config
  const filepath = '../../data/entities/raw/usuarios.csv';
  const { rows } = await importFile(filepath, false);
  const dataList = [];
  const { Region, Division, Zone, Section, User, Role } = db;
  try {
    for (const [row] of rows) {
      const [
        ,
        dni,
        resellerNumber,
        name,
        regionCode,
        divisionCode,
        zoneCode,
        sectionCode,

        ,
        ,
        roleCode,
        createdAt,
        updatedAt
      ] = row.split('\t');

      if (doesNotExist(dni)) {
        throw new Error('DNI does not exist');
      }
      const cifrado = cifrar(dni);

      const userData = {
        dni,
        username: dni,
        resellerNumber,
        name,
        password: encrypt( cifrado)
      };

      console.log("JHUCK: ", resellerNumber, ";", name, ";", dni, ";", zoneCode, ";", sectionCode, ";", roleCode, ";", cifrado );

      // TODO: join relations
      if (exists(regionCode)) {
        const region = await Region.findOne({
          where: { code: regionCode }
        });
        userData.regionId = (region === null) ? null : region.id;
      }
      if (exists(divisionCode)) {
        const division = await Division.findOne({ where: { code: divisionCode } });
        if (exists(division)) {
          userData.divisionId = division.id;
        }
      }
      if (exists(zoneCode)) {
        const zone = await Zone.findOne({ where: { code: zoneCode } });
        if (exists(zone)) {
          userData.zoneId = zone.id;
        }
      }
      if (exists(sectionCode)) {
        const section = await Section.findOne({ where: { code: sectionCode } });
        if (exists(section)) {
          userData.sectionId = section.id;
        }
      }
      if (exists(roleCode)) {
        const role = await Role.findOne({ where: { code: roleCode } });
        if (exists(role)) {
          userData.roleId = role.id;
        }
      }
      dataList.push(userData);
    }
    spinner.text = `Inserting '${dataList.length}' users...`;
    await User.bulkCreate(dataList);
  } catch (error) {
    spinner.fail(`Error: \n${error}`);
    return next(
      createError(FAILED_DEPENDENCY, error, {
        message: 'Users import failed'
      })
    );
  }
  spinner.succeed(`${startText} | done`);
  return res.json({
    status: 'done',
    message: 'Users successfully imported'
  });
};
