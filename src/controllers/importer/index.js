import config from 'config';
import createError from 'http-errors';
import { FAILED_DEPENDENCY } from 'http-status-codes';
import moment from 'moment';
import ora from 'ora';
import blessed from 'blessed';
import path from 'path';
import chalk from 'chalk';
import db from '../../models';
import { chunks } from '../../helpers/parsing';
import { fileToMatrix } from './common';
import { getDetailInserts, getHeaderInserts } from './orders';
import { getOrdersFiles, renameOrderFiles, setupImportsDirectoryTree } from '../../helpers/files';
import { doesNotExist, exists } from '../../helpers/assertion';

/**
 * Import header files
 *
 * @param {Array} paths
 * @param {String} orderType
 * @param {Boolean} removeDuplicates
 *
 * @return {Object<Array, Array>}
 */
export const importOrderFiles = async (
  paths,
  orderType = 'header',
  removeDuplicates = true,
  spinner = null
) => {
  const startText = 'Parse files ';
  if (doesNotExist(spinner)) {
    spinner = ora(startText).start();
  }

  const failedFiles = [];
  const importedFiles = [];
  const insertErrors = [];
  let relationshipErrors = [];
  const model = orderType === 'header' ? 'OrderHead' : 'OrderDetail';

  if (!['header', 'detail'].includes(orderType)) {
    throw new Error("Import orders only supports two types: ['header', 'detail']");
  }

  for (const filepath of paths) {
    const filename = path.basename(filepath);
    const fileStartText = `Parse file ${filename}`;
    spinner.text = fileStartText;
    spinner.start(`Parse file '${filename}' to matrix...`);
    let data = await fileToMatrix(filepath);
    if (removeDuplicates) {
      spinner.text = 'Remove duplicates...';
      const filteredData = [];
      data.forEach(row =>
        filteredData.map(d => d[2]).includes(row[2]) ? null : filteredData.push(row)
      );
      data = filteredData;
    }
    // Split for Sequelize connections
    const splittedData = chunks(data, config.orderImports.maxRowsPerParserSet);
    const { rows, detailsWithOutHeaders } =
      orderType === 'header'
        ? await getHeaderInserts(splittedData, spinner)
        : await getDetailInserts(splittedData, spinner);
    if (exists(detailsWithOutHeaders)) {
      relationshipErrors = [...relationshipErrors, ...detailsWithOutHeaders];
    }

    const bulkCreateOptions = { returning: true };
    if (orderType === 'header') {
      bulkCreateOptions.updateOnDuplicate = [
        'zoneCode',
        'sectionCode',
        'name',
        'address',
        'city',
        'provinceId',
        'province',
        'phone',
        'phone2',
        'dateOfBirth',
        'birthday',
        'campaignDebt',
        'campaignYearDebt',
        'createdAt',
        'updatedAt',
        'ZoneId',
        'SectionId',
        'sectionid',
        'zoneid',
        'dni'
      ];
    } else {
      bulkCreateOptions.updateOnDuplicate = [
        'cosmetics',
        'extraCosmetics',
        'currentDebt',
        'payment',
        'repayment',
        'dismantling',
        'indicated',
        'debtYear',
        'debtCampaignNumber',
        'preInvoice',
        'deposit'
      ];
    }

    spinner.text = `Insert ${model} rows...`;
    const splittedRows = chunks(rows, config.orderImports.maxInsertsPerQuery);
    try {
      for (const [index, set] of splittedRows.entries()) {
        spinner.text = `Inserting ${model} rows set ${index + 1}/${splittedRows.length}...`;
        console.log("PRINT SET")
        console.log(set)
        try {
          await db[model].bulkCreate(set, bulkCreateOptions);
        } catch (error) {
          spinner.fail(
            `Error when importing file ${chalk.red(
              filename
            )}: ${model} bulk insert failed for set ${index + 1}`
          );

          spinner.text = `Inserting ${model} rows set ${index + 1}/${
            splittedRows.length
          } | DEBUG mode: 1 by 1`;
          const { success, row, error: byOneError } = await insertByOne(set, model);
          if (!success) {
            insertErrors.push({
              row,
              error: byOneError,
              setNumber: index + 1
            });
            spinner.fail(
              `Error found for row ${chalk.red(row)}: ${model} bulk insert failed for set ${
                index + 1
              }`
            );
            console.error(byOneError);
            throw byOneError;
          }
        }
      }
      importedFiles.push(filepath);
      spinner.succeed(`${fileStartText} | Inserted ${chalk.green.bold(rows.length)} rows`);
    } catch (error) {
      failedFiles.push(filepath);
    }
  }
  spinner.succeed(`${startText} ${chalk.green('done')}`);
  return {
    importedFiles,
    failedFiles,
    insertErrors,
    relationshipErrors
  };
};

/**
 * Import order
 *
 * @returns {Promise<unknown[]>}
 */
export const importOrders = async (req, res, next) => {
  const currentDateTime = moment();
  ora(chalk.blue.bold('*** Orders file import job started ***')).info();
  // TODO: implement FTP connection

  setupImportsDirectoryTree();
  const scanStartText = 'Scan directory for files';
  const spinner = ora(scanStartText).start();
  const { headerFilePaths, detailFilePaths } = getOrdersFiles();
  spinner.succeed(`${scanStartText} ${chalk.green('done')}`);

  const importHeaders = headerFilePaths.length > 0;
  const importDetails = detailFilePaths.length > 0;

  if (importHeaders) {
    spinner.info(`Identified ${chalk.cyan(headerFilePaths.length)} Header files: `);
    spinner.info(`${headerFilePaths.map(f => chalk.cyan(path.basename(f))).join(' | ')}`);
  }
  if (importDetails) {
    spinner.info(`Identified ${chalk.cyan(detailFilePaths.length)} Detail files: `);
    spinner.info(`${detailFilePaths.map(f => chalk.cyan(path.basename(f))).join(' | ')}`);
  }

  const errors = {
    headerErrors: [],
    detailErrors: []
  };
  let relationshipErrors = [];

  try {
    if (importHeaders) {
      spinner.start('Import orders headers...');
      const {
        importedFiles: importedHeaders,
        failedFiles: failedHeaders,
        insertErrors: headerErrors
      } = await importOrderFiles(headerFilePaths, 'header', true, spinner);
      errors.headerErrors = headerErrors;
      if (failedHeaders.length > 0) {
        spinner.fail(
          `Headers import error: ${chalk.bold.red(
            failedHeaders.length
          )} files with errors were detected`
        );
      }
      spinner.succeed(`Import orders headers... ${chalk.green('done')}`);
      if (importedHeaders.length > 0) {
        spinner.succeed(
          `Imported ${chalk.bold.green(importedHeaders.length)}/${
            headerFilePaths.length
          } header files`
        );
      }
      renameOrderFiles(importedHeaders, failedHeaders);
    }
    if (importDetails) {
      spinner.start('Importing details...');
      const {
        importedFiles: importedDetails,
        failedFiles: failedDetails,
        insertErrors: detailErrors,
        relationshipErrors: detailRelationshipErrors
      } = await importOrderFiles(detailFilePaths, 'detail', false, spinner);
      errors.detailErrors = detailErrors;
      relationshipErrors = detailRelationshipErrors;
      if (failedDetails.length !== 0) {
        spinner.fail(
          `Details import error: ${chalk.bold.red(
            failedDetails.length
          )} files with errors were detected`
        );
      }
      if (importedDetails.length > 0) {
        spinner.succeed(
          `Imported ${chalk.bold.green(importedDetails.length)}/${
            detailFilePaths.length
          } detail files`
        );
      }
      renameOrderFiles(importedDetails, failedDetails);
    }
  } catch (error) {
    spinner.fail('Orders file import job failed');
    spinner.fail(error);
    return next(
      createError(FAILED_DEPENDENCY, error, {
        message: 'Orders import failed'
      })
    );
  }
  spinner.info('Done! Import orders job finished.');
  spinner.info(
    `Total time elapsed: ${moment().diff(currentDateTime, 'minutes')} minutes (approximate)`
  );
  return res.json({
    status: 'done',
    message: 'Orders successfully imported',
    errors,
    relationshipErrors
  });
};

export const insertByOne = async (set, model) => {
  for (const row of set) {
    try {
      await db[model].create(row, { returning: true });
    } catch (error) {
      return { success: false, row, error };
    }
  }
  return { success: true };
};

export default importOrders;
