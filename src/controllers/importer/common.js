import fs from 'fs';
import readline from 'readline';

export const fileToMatrix = async (filepath, separator = ';', skipFirst = false) => {
  const fileStream = fs.createReadStream(filepath, { encoding: 'latin1' });
  const rl = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity
  });
  const data = [];

  let first = skipFirst;
  for await (const line of rl) {
    if (!first) {
      data.push(line.split(separator));
    }
    first = false;
  }
  return data;
};
