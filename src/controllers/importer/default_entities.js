import fs from 'fs';
import path from 'path';
import { parse } from 'fast-csv';

import ora from 'ora';
import chalk from 'chalk';
import createError from 'http-errors';
import { FAILED_DEPENDENCY } from 'http-status-codes';
import { defaultDivisions, defaultRegions, defaultRoles } from '../../data/entities';
import db from '../../models';

export const importFile = (filepath, headers = true) =>
  new Promise((resolve, reject) => {
    const rows = [];
    fs.createReadStream(path.resolve(__dirname, filepath))
      .pipe(parse({ headers }))
      .on('error', reject)
      .on('data', row => rows.push(row))
      .on('end', count => resolve({ rows, count }));
  });

export const importRegions = (rows = defaultRegions) => db.Region.bulkCreate(rows);

export const importDivisions = (rows = defaultDivisions) => db.Division.bulkCreate(rows);

export const importRoles = (rows = defaultRoles) => db.Role.bulkCreate(rows);

export const syncZoneSection = async (
  zoneCode,
  sectionCode,
  spinner = ora('Sync Zone-Section').start(),
  prefixText
) => {
  spinner.text = `${prefixText} Synchronize section '${chalk.bold.cyan(
    sectionCode
  )}' and zone '${chalk.bold.cyan(zoneCode)}'`;
  const [[section], [zone]] = await Promise.all([
    db.Section.findOrCreate({
      where: { code: sectionCode },
      defaults: {
        title: sectionCode
      }
    }),
    db.Zone.findOrCreate({
      where: { code: zoneCode },
      defaults: {
        title: zoneCode
      }
    })
  ]);
  if (await zone.hasSection(section)) {
    return Promise.resolve();
  }
  return zone.addSection(section);
};

export const importZoneSections = async (
  filepath = '../../data/entities/raw/zone_sections.csv'
) => {
  const prefixText = 'Import Zone-Sections Task: ';
  const spinner = ora({ text: `${prefixText}Starting...` }).start();
  const { rows } = await importFile(filepath);

  for (const [i, r] of rows.entries()) {
    const zoneCode = r.code.slice(0, 3);
    const sectionCode = r.code.slice(3, 6);
    await syncZoneSection(zoneCode, sectionCode, spinner, `[${i}/${rows.length}]`);
  }
  spinner.succeed(`${prefixText} | ${chalk.green('done')}`);
  spinner.info(`Synchronized ${chalk.blue(rows.length)} primary entities rows`);
  return Promise.resolve();
};

export const relatePrimaryEntities = async (
  filepath = '../../data/entities/raw/zone_div_reg_subreg.csv'
) => {
  const startText = 'Synchronize primary entities...';
  const spinner = ora(startText).start();
  const { rows } = await importFile(filepath);
  for (const [
    i,
    { zone: zoneCode, division: divisionCode, region: regionCode }
  ] of rows.entries()) {
    spinner.text = `[${i}/${rows.length}] Relating {zone: '${zoneCode}', division: '${divisionCode}', region: '${regionCode}'}...`;
    const [zone, division, region] = await Promise.all([
      db.Zone.findOne({ where: { code: zoneCode } }),
      db.Division.findOne({ where: { code: divisionCode } }),
      db.Region.findOne({ where: { code: regionCode } })
    ]);

    const [hasZone, hasDivision] = await Promise.all([
      division.hasZone(zone),
      region.hasDivision(division)
    ]);
    if (!hasZone) {
      await division.addZone(zone);
    }
    if (!hasDivision) {
      await region.addDivision(division);
    }
  }
  spinner.succeed(`${startText} | ${chalk.green('done')}`);
  spinner.info(`Synchronized ${chalk.blue(rows.length)} primary entities rows`);
  return Promise.resolve();
};

export const importDefaults = async (req, res, next) => {
  const startText = 'Import default entities job started';
  const spinner = ora(startText).info();
  try {
    await Promise.all([importRegions(), importDivisions(), importZoneSections(), importRoles()]);
  } catch (error) {
    spinner.fail('Import failed with error');
    console.error(error);
    return next(
      createError(FAILED_DEPENDENCY, error, {
        message: 'Import default entities job failed'
      })
    );
  }
  try {
    await relatePrimaryEntities();
  } catch (error) {
    spinner.fail('Relate primary entities failed with error');
    console.error(error);
    return next(
      createError(FAILED_DEPENDENCY, error, {
        message: 'Import default entities job failed'
      })
    );
  }
  spinner.succeed(`${chalk.green('done')} | ${startText}`);
  return res.json({
    status: 'done',
    message: 'Defaults successfully imported'
  });
};
