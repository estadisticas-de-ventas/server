import ora from 'ora';
import chalk from 'chalk';
import db from '../../models';
import { doesNotExist, exists } from '../../helpers/assertion';

export const getDetailRowsFromSet = async dataset => {
  const detailsWithOutHeaders = [];
  const rows = [];
  for (const row of dataset) {
    const modelRow = db.OrderDetail.rawRowToModel(row);
    const res = await db.OrderHead.findOne({
      where: { resellerNumber: modelRow.resellerNumber },
      raw: true
    });
    if (doesNotExist(res) || doesNotExist(res.id)) {
      detailsWithOutHeaders.push(row);
    } else {
      modelRow.orderHeadId = res.id;
      
      rows.push(modelRow);
    }
  }
  return { rows, detailsWithOutHeaders };
};

export const getHeaderInserts = async (data, spinner = null) => {
  if (doesNotExist(spinner)) {
    spinner = ora('Parse OrderHeader sets...').start();
  }
  let rows = [];
  for (const [index, set] of data.entries()) {
    spinner.text = `[${index + 1}/${data.length}] Parsing OrderHeader set ${chalk.cyan(index + 1)}`;
    const localRows = await Promise.all(
      set.map(async row => {
        const modelRow = db.OrderHead.rawRowToModel(row);
        const section = await db.Section.findOne({ where: { code: modelRow.sectionCode } });
        const zone = await db.Zone.findOne({ where: { code: modelRow.zoneCode } });

        if (section !== null) {
          modelRow.section_id = section.id;
        }
        else{
          //si en la base inserta como null es porque aca habria que realizar
          //el tema del insert, pero aca no tengo divisiones ni regiones.
          //
          //insert section
          //modelRow.sectionCode
          //section = await db.Section.findOne({ where: { code: modelRow.sectionCode } });
        }

        if (zone !== null) {
          modelRow.zone_id = zone.id;
        }
        else{
          //si en la base inserta como null es porque aca habria que realizar
          //insert zone
          //modelRow.zoneCode
         // db.Zone.create(option)
         // zone = await db.Zone.findOne({ where: { code: modelRow.zoneCode } });
        }

        return modelRow;
      })
    );
    const filteredLocalRows = localRows.filter(r => r !== null);
    rows = [...rows, ...filteredLocalRows];
  }
  spinner.text = `Parse OrderHeader sets... ${chalk.green('done')}`;
  return { rows };
};

export const getDetailInserts = async (data, spinner = null) => {
  if (doesNotExist(spinner)) {
    spinner = ora('Parse OrderDetail sets...').start();
  }
  let rows = [];
  let detailsWithOutHeaders = [];
  for (const [index, set] of data.entries()) {

    spinner.text = `[${index + 1}/${data.length}] Parsing OrderDetail set ${chalk.cyan(index + 1)}`;
    const { rows: localRows, detailsWithOutHeaders: noHeaders } = await getDetailRowsFromSet(set);
    rows = [...rows, ...localRows];

    detailsWithOutHeaders = [...detailsWithOutHeaders, ...noHeaders];
  }
  spinner.text = `Parse OrderDetail sets... ${chalk.green('done')}`;

  return { rows, detailsWithOutHeaders };
};
