import createError from 'http-errors';
import { FAILED_DEPENDENCY } from 'http-status-codes';
import { QueryTypes } from 'sequelize';
import db from '../models';
import { exists } from '../helpers/assertion';
import OrderHead from '../models/order_head';

import { getCampaigns, getPreviousCampaign } from './campaigns';
import { getFilterRangeSortQueries } from '../helpers/request';

import { customizeFilters } from './orders';

export const list = async (req, res, next) => {};

const getCampaignSimpleTotals = async (campaign, campaignYear, headerIds = null) => {
  const query = `
      SELECT count(DISTINCT order_head_id)::integer                                       as "totalRows",\
       SUM(cosmetics)                                                                     as "cosmetics",\
       SUM(extra_cosmetics)                                                               as "extraCosmetics",\
       SUM(current_debt)                                                                  as "currentDebt",\
       SUM(CASE WHEN dismantling = 'D' THEN 1 ELSE 0 END)::integer                        as "dismantling",\
       SUM(CASE WHEN payment = 'PC' THEN 1 ELSE 0 END)::integer                           as "payment"\
       FROM order_details WHERE campaign = :campaign AND campaign_year = :campaignYear AND pre_invoice = 'f'\
       ${exists(headerIds) ? `AND order_head_id IN (${headerIds.join(', ')})` : ''};
      `.replace(/\n/g, '');
  const [
    { cosmetics, extraCosmetics, totalRows, currentDebt, dismantling, payment }
  ] = await db.sequelize.query(query, {
    replacements: { campaign, campaignYear },
    type: QueryTypes.SELECT
  });
  return {
    cosmetics,
    currentDebt,
    dismantling,
    payment,
    extraCosmetics,
    totalCosmetics: parseInt(cosmetics, 10) + parseInt(extraCosmetics, 10),
    totalRows
  };
};

export const countReincos = async (
  fromCampaign,
  fromCampaignYear,
  toCampaign,
  toCampaignYear,
  headerIds = null
) => {
  const headerIdsQuery = exists(headerIds) ? `AND h.id IN (${headerIds.join(', ')})` : '';

  const query = `
        SELECT count(h.*)::integer
        FROM order_heads h
        WHERE NOT EXISTS(SELECT
        FROM order_details v
        WHERE v.reseller_number::integer = h.reseller_number::integer
          AND v.campaign >= :fromCampaign AND v.campaign <= :toCampaign
          AND v.campaign_year >= :fromCampaignYear AND v.campaign_year <= :toCampaignYear) ${headerIdsQuery};
      `.replace(/\n/g, '');
  const [{ count }] = await db.sequelize.query(query, {
    replacements: {
      fromCampaign,
      fromCampaignYear,
      toCampaign,
      toCampaignYear
    },
    type: QueryTypes.SELECT
  });
  return count;
};

export const countComebacks = async (
  fromCampaign,
  fromCampaignYear,
  toCampaign,
  toCampaignYear,
  headerIds = null
) => {
  const headerIdsQuery = exists(headerIds) ? `AND h.id IN (${headerIds.join(', ')})` : '';
  // TODO: fixear campanias segun numero
  const [{ count }] = await db.sequelize.query(
    `
        SELECT count(h.*)::integer
        FROM order_heads h
        WHERE NOT EXISTS(SELECT
          FROM order_details v
          WHERE v.reseller_number::integer = h.reseller_number::integer
            AND v.campaign >= :fromCampaign AND v.campaign <= :preCampaign
            AND v.campaign_year >= :fromCampaignYear AND v.campaign_year <= :preCampaignYear)
        AND EXISTS(SELECT
          FROM order_details v
          WHERE v.reseller_number::integer = h.reseller_number::integer
            AND v.campaign::integer = :toCampaign AND v.campaign_year::integer = :toCampaignYear) ${headerIdsQuery};
      `.replace(/\n/g, ''),
    {
      replacements: {
        fromCampaign,
        fromCampaignYear,
        preCampaign: toCampaign - 1,
        preCampaignYear: toCampaignYear,
        toCampaign,
        toCampaignYear
      },
      type: QueryTypes.SELECT
    }
  );
  return count;
};

export const countFirstTimers = async (toCampaign, toCampaignYear, headerIds = null) => {
  const headerIdsQuery = exists(headerIds) ? `AND h.id IN (${headerIds.join(', ')})` : '';

  const query = `
        SELECT count(h.*)::integer
        FROM order_heads h
        WHERE NOT EXISTS(SELECT
          FROM order_details v
          WHERE v.reseller_number::integer = h.reseller_number::integer
            AND v.campaign < :toCampaign
            AND v.campaign_year <= :toCampaignYear)
        AND EXISTS(SELECT
          FROM order_details v
          WHERE v.reseller_number::integer = h.reseller_number::integer
            AND v.campaign::integer = :toCampaign AND v.campaign_year::integer = :toCampaignYear) ${headerIdsQuery};
      `.replace(/\n/g, '');
  // TODO: Fix desde hasta
  const [{ count }] = await db.sequelize.query(query, {
    replacements: {
      toCampaign,
      toCampaignYear
    },
    type: QueryTypes.SELECT
  });
  return count;
};

/**
 * Get campaign debt
 *
 * @param {String} campaign               - Campaign number
 * @param {String} campaignYear           - Campaign year
 * @param {Array<Number>} headerIds       - Header IDs
 * @return {Promise<*>}
 */
export const getCampaignDebt = async (campaign, campaignYear, headerIds = null) => {
  const headerIdsQuery = exists(headerIds) ? `AND order_head_id IN (${headerIds.join(', ')})` : '';
  const [{ campaignDebt }] = await db.sequelize.query(
    `
      SELECT SUM(current_debt) as "campaignDebt"
      FROM order_details WHERE campaign = :campaign AND campaign_year = :campaignYear ${headerIdsQuery};
      `.replace(/\n/g, ''),
    {
      replacements: { campaign, campaignYear },
      type: QueryTypes.SELECT
    }
  );
  return campaignDebt;
};

const getCampaignTotals = async (campaigns, campaignNumber, campaignYear, headerIds = null) => {
  const index = campaigns.findIndex(
    c => c.campaign === campaignNumber && c.campaignYear === campaignYear
  );
  const n = campaigns.length;
  const promises = [
    getCampaignSimpleTotals(campaignNumber, campaignYear, headerIds),
    index + 1 < n
      ? countFirstTimers(campaigns[index].campaign, campaigns[index].campaignYear, headerIds)
      : 0,
    index + 1 < n
      ? getCampaignDebt(campaigns[index + 1].campaign, campaigns[index + 1].campaignYear, headerIds)
      : 0,
    index + 2 < n
      ? getCampaignDebt(campaigns[index + 2].campaign, campaigns[index + 2].campaignYear, headerIds)
      : 0,
    index + 3 < n
      ? getCampaignDebt(campaigns[index + 3].campaign, campaigns[index + 3].campaignYear, headerIds)
      : 0,
    index + 3 < n
      ? countReincos(
          campaigns[index + 3].campaign,
          campaigns[index + 3].campaignYear,
          campaignNumber,
          campaignYear,
          headerIds
        )
      : 0,
    index + 6 < n
      ? countComebacks(
          campaigns[index + 6].campaign,
          campaigns[index + 6].campaignYear,
          campaignNumber,
          campaignYear,
          headerIds
        )
      : 0
  ];
  const [
    { cosmetics, currentDebt, dismantling, payment, extraCosmetics, totalCosmetics, totalRows },
    firstOrders,
    debtPast,
    debtPast2,
    debtPast3,
    reincos,
    comebacks
  ] = await Promise.all(promises);
  return {
    cosmetics,
    currentDebt,
    dismantling,
    payment,
    extraCosmetics,
    totalCosmetics,
    totalRows,
    reincos,
    comebacks,
    firstOrders,
    debtPast,
    debtPast2,
    debtPast3
  };
};

export const get = async (req, res, next) => {
  delete req.query.sort;
  delete req.query.range;
  const queryOptions = getFilterRangeSortQueries(req);

  if (exists(queryOptions.filter) && exists(queryOptions.filter.dateOfBirthFrom)) {
    queryOptions.filter.dateOfBirthFrom = `1111${queryOptions.filter.dateOfBirthFrom.slice(4)}`;
  }
  if (exists(queryOptions.dateOfBirthTo)) {
    queryOptions.filter.dateOfBirthTo = `9999${queryOptions.filter.dateOfBirthTo.slice(4)}`;
  }
  const { sequelizeOptions } = customizeFilters(queryOptions);

  const { campaignNumber } = req.params;

  const campaignYear = Number(campaignNumber.substring(0, 4));
  const campaign = Number(campaignNumber.substring(4));

  const response = { id: 1, current: {}, previous: {} };

  const { count, rows: headers } = await OrderHead.findAndCountAll(sequelizeOptions);
  const headerIds = headers.map(({ id }) => id);

  try {
    const campaignList = await getCampaigns('DESC');
    const selectedCampaignIndex = campaignList.findIndex(
      c => c.campaign === campaign && c.campaignYear === campaignYear
    );

    const promises = [getCampaignTotals(campaignList, campaign, campaignYear, headerIds)];

    if (selectedCampaignIndex + 1 < campaignList.length) {
      const previousCampaignData = campaignList[selectedCampaignIndex + 1];
      promises.push(
        getCampaignTotals(
          campaignList,
          previousCampaignData.campaign,
          previousCampaignData.campaignYear,
          headerIds
        )
      );
    }
    const [current, previous] = await Promise.all(promises);

    response.current = current;
    if (exists(previous)) {
      response.previous = previous;
    }

    // Fix for react admin
    // Having to do this, sucks, really sucks. But is the only way that React Admin provided will execute right
    res.set('Content-Range', 'orders/totals 1-1/1');

    return res.json([response]);
  } catch (error) {
    return next(
      createError(FAILED_DEPENDENCY, error, {
        message: `Get Totals for campaign '${campaignNumber}' failed`
      })
    );
  }
};
