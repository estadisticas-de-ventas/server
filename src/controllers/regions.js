import createError from 'http-errors';
import { FAILED_DEPENDENCY } from 'http-status-codes';
import { getFilterRangeSortQueries, reqOptionsQueriesToSequelize } from '../helpers/request';
import { setContentRangeHeader } from '../helpers/pagination';
import db from '../models';

export const list = async (req, res, next) => {
  const queryOptions = getFilterRangeSortQueries(req);
  const sequelizeOptions = reqOptionsQueriesToSequelize(queryOptions, 'Region');
  let records = [];
  let totalRecords = 0;
  const { Region } = db;
  try {
    const { count, rows } = await Region.findAndCountAll(sequelizeOptions);
    records = rows;
    totalRecords = count;
  } catch (error) {
    return next(
      createError(FAILED_DEPENDENCY, error, {
        message: 'List Region collection failed'
      })
    );
  }
  setContentRangeHeader(queryOptions, res, totalRecords);
  return res.json(records);
};

export const get = async (req, res, next) => {
  const { id } = req.params;
  const { Region } = db;
  try {
    const region = await Region.findByPk(id);
    return res.json(region);
  } catch (error) {
    return next(
      createError(FAILED_DEPENDENCY, error, { message: `Get Region '${id}' collection failed` })
    );
  }
};
