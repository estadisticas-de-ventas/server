import * as expressWinston from 'express-winston';
import * as winston from 'winston';
import bodyParser from 'body-parser';
import config from 'config';
import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import chalk from 'chalk';
import { transports, format } from 'winston';
import routes from './routes';
import { requestFilter } from './helpers/request';
import { setErrorHandlerMiddleware } from './helpers/errors';
import './helpers/cronJobs'; // importa las tareas programadas (cron jobs)


const app = express();
const path = require('path');

// Configure Header HTTP
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method"
  );
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE");
  next();
});

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'))
app.use(bodyParser.json());
app.set('etag', false); // turn off
app.use(morgan('dev'));
app.use(
  expressWinston.logger({
    transports: [
      new transports.Console(),
      new transports.File({
        filename: config.log.combinedFilename,
        level: 'info'
      }),
      new transports.File({
        filename: config.log.errorFilename,
        level: 'error'
      })
    ],
    format: format.combine(format.errors({ stack: true }), format.colorize(), format.json()),
    msg: 'HTTP {{req.method}} {{req.url}}',
    expressFormat: true,
    colorize: true,
    meta: false,
    requestFilter
  })
);
app.use(config.server.apiUrlPrefix, routes);
app.use(
  expressWinston.errorLogger({
    transports: [
      new winston.transports.Console(),
      new winston.transports.File({
        filename: config.log.combinedFilename,
        level: 'info'
      }),
      new winston.transports.File({
        filename: config.log.errorFilename,
        level: 'error'
      })
    ],
    format: winston.format.combine(
      format.errors({ stack: true }),
      winston.format.colorize(),
      winston.format.json()
    ),
    dumpExceptions: true,
    showStack: true
  })
);

app.use(setErrorHandlerMiddleware(app.get('env')));

const port = process.env.PORT || config.server.port;
app.listen(port);
console.log(
  `${chalk.cyan('Violetta REST API server started on port: ')}${chalk.blue.bold(port)}`
);

export default app;
