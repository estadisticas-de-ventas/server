module.exports = {
  server: {
    port: 9000,
    apiUrlPrefix: process.env.API_URL_PREFIX || ''
  },
  database: {
    username: process.env.DB_USERNAME || 'postgres',
    password: process.env.DB_PASSWORD || 'postgres',
    host: process.env.DB_HOST || 'localhost',
    port: process.env.DB_PORT || 9432,
    database: process.env.DB_DATABASE || 'violetta'
  },
  pagination: {
    defaultPage: 1,
    defaultLimit: 10
  },
  axiosRequestTimeout: 2000,
  log: {
    combinedFilename: process.env.LOG_COMBINED_FILENAME || 'combined.log',
    errorFilename: process.env.LOG_ERROR_FILENAME || 'error.log'
  },
  orderImports: {
    importsDirectory: process.env.IMPORTS_DIRECTORY || 'docs/client_data/import_txt/',
    prefixes: {
      header: 'cabecera_',
      detail: 'detalle_'
    },
    maxInsertsPerQuery: process.env.MAX_INSERTS_PER_QUERY || 1000,
    maxRowsPerParserSet: process.env.MAX_ROWS_PER_PARSER_SET || 50
  },
  defaults: {
    users: {
      password: '12341234'
    }
  },
  crypto: { algorithm: 'aes-256-ctr', password: 'd6F3Efeq1515848aw2' },
  tokenExpirationTime: process.env.TOKEN_EXPIRATION_TIME || 604800,
  secretKey: process.env.UC_SECRET || 'waver-barely-insurer-preterit-wales-eerily'
};
