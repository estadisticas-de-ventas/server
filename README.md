# Violetta server REST API
This project contains the main Violetta API, aiming to centralize every REST/database related task. 

## Getting started

### Prerequisites
* git: http://git-scm.com/.
* Node.js and npm (node.js package manager): http://nodejs.org/.
* Yarn: `pacman -S yarn` or https://classic.yarnpkg.com/en/docs/install
* (optional) PostgreSQL server & PSQL cli tool: https://www.postgresql.org/download/

### Development environment

1. Clone the `violetta-server` repository using git:
    ```
    git clone git@github.com:ngoldenberg/violetta-server.git
    ```
2. Install dependencies:
    ```
    yarn install
    ```

3. Run the application:
    ```
    yarn start
    ```

Note: make sure Postgres db is up and running on your machines.

### Development database
Development database can be interacted with via Sequelize CLI:
```
Sequelize CLI [Node: 10.21.0, CLI: 6.0.0, ORM: 6.1.0]

sequelize <command>

Commands:
  sequelize db:migrate                        Run pending migrations
  sequelize db:migrate:schema:timestamps:add  Update migration table to have timestamps
  sequelize db:migrate:status                 List the status of all migrations
  sequelize db:migrate:undo                   Reverts a migration
  sequelize db:migrate:undo:all               Revert all migrations ran
  sequelize db:seed                           Run specified seeder
  sequelize db:seed:undo                      Deletes data from the database
  sequelize db:seed:all                       Run every seeder
  sequelize db:seed:undo:all                  Deletes data from the database
  sequelize db:create                         Create database specified by configuration
  sequelize db:drop                           Drop database specified by configuration
  sequelize init                              Initializes project
  sequelize init:config                       Initializes configuration
  sequelize init:migrations                   Initializes migrations
  sequelize init:models                       Initializes models
  sequelize init:seeders                      Initializes seeders
  sequelize migration:generate                Generates a new migration file      [aliases: migration:create]
  sequelize model:generate                    Generates a model and its migration [aliases: model:create]
  sequelize seed:generate                     Generates a new seed file           [aliases: seed:create]

Options:
  --version  Show version number                                                  [boolean]
  --help     Show help                                                            [boolean]

Please specify a command
```

For installation and usage instructions, please refer to [this doc](https://github.com/sequelize/cli#usage).

### Environmental variables
#### Database
* DB_USERNAME: Database server username
* DB_PASSWORD: Database server password
* DB_HOST: Database server host
* DB_PORT: Database server port
* DB_DATABASE: Database name

#### Imports
* IMPORTS_DIRECTORY: Directory to read import files from
* MAX_INSERTS_PER_QUERY: Max number of inserts per query (used by `creatBulk` method)
* MAX_ROWS_PER_PARSER_SET: Max number of rows to be parsed per set/batch

#### Others
* API_URL_PREFIX: Prefix for API routes
* LOG_COMBINED_FILENAME: Combined log filename
* LOG_ERROR_FILENAME: Error log filename
* TOKEN_EXPIRATION_TIME: Token expiration time
* UC_SECRET: JWT secret

## Production build
This project provides a Dockerfile in order to correctly build a production image, enabling its usage inside a `docker-compose` file.
* Install docker: https://www.docker.com/
* Build & tag:
    ```
    docker build -t ngoldenberg/violetta:api-dev.1 .
    ```
* Push to docker registry:
    ```
    docker push ngoldenberg/violetta:api-dev.1
    ```
  
Notice: docker build exposes port `9000`.

## Additional hazards
### Code
#### ECMAScript® 2019
ECMAScript 2019 standard is **enforced**.
All `stage-3` experimental features are enable. 

### Node JS
#### Babel
Notice `babel` compiles the code instead of using plain NodeJs?
This is required by ECMAScript 2019 and ESLint in order to correctly compile (and lint) the application, thus enabling cool features such as private class methods and properties. 

### Endpoints
The Postman REST collection file keeps track of every endpoint, parameters, auth, etc...
* Install Postman: https://www.postman.com/
* Import `violetta.postman_collection.json` file

## CAMBIOS EN LA DB

Listado de cambios que hay que tener en la DB para que el proyecto funcione:

```
ALTER TABLE order_details ADD COLUMN deposit varchar(255);
ALTER TABLE order_details ADD COLUMN tickets_ecommerce integer;
```

## References
 * [Node + Express REST API skeleton](https://github.com/romandunets/node-express-skeleton)
 * [Best Practices for Designing a Pragmatic RESTful API](http://www.vinaysahni.com/best-practices-for-a-pragmatic-restful-api#pagination)
 * [Best practices for Express app structure](https://www.terlici.com/2014/08/25/best-practices-express-structure.html)
 * [User Authentication using JWT (JSON Web Token) in Node.js (Express Framework)](https://medium.com/@pandeysoni/user-authentication-using-jwt-json-web-token-in-node-js-using-express-framework-543151a38ea1)
 * [Authenticate a Node.js API with JSON Web Tokens](https://scotch.io/tutorials/authenticate-a-node-js-api-with-json-web-tokens)
 * [5 Steps to Authenticating Node.js with JWT](https://www.codementor.io/olatundegaruba/5-steps-to-authenticating-node-js-with-jwt-7ahb5dmyr)
 * [Route Middleware to Check if a User is Authenticated in Node.js](https://scotch.io/quick-tips/route-middleware-to-check-if-a-user-is-authenticated-in-node-js)
 * [Keeping API Routing Clean Using Express Routers](https://scotch.io/tutorials/keeping-api-routing-clean-using-express-routers)
